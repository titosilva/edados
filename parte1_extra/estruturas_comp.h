// Esse arquivo será utilizado para declaração das estruturas a serem utilizadas no problema
#ifndef PROB_ESTRUTURAS_H__
#define PROB_ESTRUTURAS_H__

#define MAXPILHA 50

// MANIPULAÇÃO DOS TITULOS DE LIVROS ==========================================================

// Estrutura que representa um livro
typedef struct livro_str{
    char *titulo;
}Livro;

// Compara dois livros com relação à ordem alfabética
// Retornos:
// 0, se os dois titulos sao iguais
// 1, se os titulos estão em ordem alfabetica(o segundo argumento tambem é o segundo titulo em ordem alfabetica)
// -1, se os titulos não estão em ordem alfabetica
int ordemAlfabetica(char const *t1, char const *t2);

// LISTA ENCADEADA ============================================================================
typedef struct elem_str_livros{
    Livro livro;
    int quantidade;
    struct elem_str_livros* prox;
}ElementoListaLivros;

typedef struct listadelivros_str{
    ElementoListaLivros *inicio;
}ListaDeLivros;

ListaDeLivros novaListaDeLivros();

// Procura pelo livro com titulo "titulo" na lista.
// Se for encontrado, retorna a posição em que foi encontrado
// Se não for encontrado, retorna o oposto da posição em que deve ser inserido subtraida de 1(ordem alfabetica)
int buscarLivroEmLista(ListaDeLivros *lista, char const* titulo);

// Adiciona elemento à lista encadeada
// Retorna 1 se o elemento já estava na lista, e não adiciona elementos repetidos
// Nesse último caso, incrementa o campo "quantidade" de ElementoListaLivros
int adicionarLivroEmLista(ListaDeLivros *lista, char const* titulo);

// Libera a memória utilizada por uma lista
void finalizarListaDeLivros(ListaDeLivros *lista);

// Mostra os titulos e as quantidades
void mostrarLista(ListaDeLivros *lista);

// PILHA ======================================================================================
typedef struct pilhadelivros_str{
    Livro livros[MAXPILHA];
    int topo;
} PilhaDeLivros;

// Inicializar pilha de livros (topo=-1)
PilhaDeLivros novaPilhaDeLivros();

// Cria pilha a partir de arquivo
PilhaDeLivros montarPilhaArq(char const* nomearq);

// Adiciona livro ao topo da pilha
// retorna 1 se foi possível adicionar o livro
// retorna 0 se não há mais espaço
int AdicionarLivroEmPilha(PilhaDeLivros *pilha, Livro livro);

// Imprime a pilha (do topo para a base)
void imprimirPilhaDeLivros(PilhaDeLivros *pilha);

// Remove livro do topo da pilha e adiciona em uma lista de livros
// retorna 1 se foi possível remover o livro
// retorna 0 se não foi possível remover o livro(se não havia livro)
int transferirParaLista(PilhaDeLivros *pilha, ListaDeLivros *lista);

// Finaliza pilha, eliminando espaço ocupado na memória
void finalizarPilha(PilhaDeLivros *pilha);

// PRATELEIRA =================================================================================
typedef struct str_prateleira{
    char *tema;
    ListaDeLivros listadelivros;
} Prateleira;

#endif