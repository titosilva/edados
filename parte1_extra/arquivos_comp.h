#ifndef PROBLEMA_ARQUIVOS_H__
#define PROBLEMA_ARQUIVOS_H__
#include <stdio.h>

// LISTA DE GERACAO ALEATORIA ==========================================================
#define LIVROS_TXT "livros.txt"

// Verifica livros.txt para saber quantos livros há na lista de geração aleatória
// Retorna -1 caso não seja encontrado o arquivo livros.txt
int quantidadeDeLivrosLT();

// Seleciona um livro dentro do arquivo de acordo com a ordem em que aparecem no arquivo
// Exemplo: 0 seleciona o primeiro arquivo que aparecer e retorna seu nome
// 2 seleciona o terceiro arquivo que aparecer e retorna seu nome
// Obs.: a função usa calloc, então lembre de dar um free no resultado retornado
char *nomeDeLivroLT(unsigned int posicao);

// FUNCOES PARA OUTROS ARQUIVOS =========================================================
// Checa se o arquivo termina com \n
// Se não tiver, adiciona \n ao final do arquivo
int adicionarNovaLinha(char const *nomearq);

// Verifica quantos livros há em um dado arquivo
// Retorna -1 caso não seja encontrado o arquivo
int quantidadeDeLivros(char const *nomearq);

// Seleciona um livro dentro do arquivo de acordo com a ordem em que aparecem no arquivo
// Exemplo: 0 seleciona o primeiro arquivo que aparecer e retorna seu nome
// 2 seleciona o terceiro arquivo que aparecer e retorna seu nome
// Obs.: a função usa calloc, então lembre de dar um free no resultado retornado
char *nomeDeLivro(char const *nomearq, unsigned int posicao);

// Retorna o tema do livro indicado
char *temaDeLivro(char const *nomelivro);


#endif