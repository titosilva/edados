// Aqui, implementaremos o problema
// PROBLEMA: Imagine que você trabalha em uma livraria e deseja organizar certos livros
// que estavam dentro de uma caixa em ordem alfábetica. Como fazer isso?

#include "estruturas_comp.h"
#include "arquivos_comp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Local onde está o arquivo com os nomes dos livros

int gerarPilhaAleatoria(char* const nome_arq, int quantidade);
void mostrarMensagemDeAjuda(char* const exe_nome);
int organizarArquivo(char const *nomearq);

int main(int argc, char* const argv[]){
    // Gerar numeros aleatorios para geração de pilhas aleatorios
    srand(time(NULL));

    if(argc==4){
        if(!strcmp(argv[1], "--aleatorio")||!strcmp(argv[1], "-a")){
            if(gerarPilhaAleatoria(argv[2], atoi(argv[3]))){
                printf("Ocorreu uma falha ao tentar gerar o arquivo :(\n");
                return 1;
            }
            else return 0;
        }else mostrarMensagemDeAjuda(argv[0]); 
    } else
    if(argc==3){
        // Gera arquivo aleatorio (o arquivo corresponde à caixa de livros)
        if(!strcmp(argv[1], "--organizar")||!strcmp(argv[1], "-o")){
            int nlivros=quantidadeDeLivros(argv[2]);
            if(nlivros==-1){
                printf("Nao foi possivel encontrar o arquivo solicitado :(\n");
            }else if(nlivros==0){
                printf("Nao foram encontrados quaisquer livros no arquivo\n");
            }else if(nlivros>MAXPILHA){
                printf("O tamanho da pilha excede o limite maximo permitido (%d livros)\n", MAXPILHA);
            }else{
                int k = organizarArquivo(argv[2]);
                if(k==1) printf("Houve uma falha ao tentar abrir o arquivo :(\n");
                else return 0;
            }
            return 1;
        }else 
        if(!strcmp(argv[2], "--organizar")||!strcmp(argv[2], "-o")){
            int nlivros=quantidadeDeLivros(argv[1]);
            if(nlivros==-1){
                printf("Nao foi possivel encontrar o arquivo solicitado :(\n");
            }else if(nlivros==0){
                printf("Nao foram encontrados quaisquer livros no arquivo\n");
            }else if(nlivros>MAXPILHA){
                printf("O tamanho da pilha excede o limite maximo permitido (%d livros)\n", MAXPILHA);
            }else{
                int k = organizarArquivo(argv[2]);
                if(k==1) printf("Houve uma falha ao tentar abrir o arquivo :(\n");
                else return 0;
            }
            return 1;
        }else mostrarMensagemDeAjuda(argv[0]);
    }else{
        mostrarMensagemDeAjuda(argv[0]);
        return 0;
    }
    
}

int gerarPilhaAleatoria(char * const nome_arq, int quantidade){
    if(quantidade<=0) return 1;
    
    FILE *result = fopen(nome_arq, "w");

    if(result==NULL) return 1;

    // Conta quantos livros diferentes há no arquivo
    int nlivros = quantidadeDeLivrosLT();

    char *livro;
    for(int i=0; i<quantidade; i++) fprintf(result, "%s\n", livro=nomeDeLivroLT(rand()%nlivros));
    free(livro);

    fclose(result);

    return 0;
}

int organizarArquivo(char const *nomearq){
    adicionarNovaLinha(nomearq);

    PilhaDeLivros pilha = novaPilhaDeLivros();
    pilha = montarPilhaArq(nomearq);
    imprimirPilhaDeLivros(&pilha);
    ListaDeLivros lista = novaListaDeLivros();

    // Array de prateleiras (cada prateleira representa um tema)
    Prateleira *prateleiras=NULL;
    // Quantidade de prateleiras
    int nplateleiras=0;

    char *tema;
    while(pilha.topo>-1){
        int i;
        // Procura, nas prateleiras, qual possui tema desse livro
        tema = temaDeLivro(pilha.livros[pilha.topo].titulo);
        if(tema==NULL){
            for(i=0; i<nplateleiras; i++) if(prateleiras[i].tema==NULL) break;
        }else {
            for(i=0; i<nplateleiras; i++){
                // Atenção: a ordem em que está organizado o if é importante nesse caso
                if(prateleiras[i].tema!=NULL){
                    if(!strcmp(tema, prateleiras[i].tema)) break;
                }
            }
        }

        // Se a prateleira com o tema indicado não for encontrada,
        // cria uma nova prateleira e adiciona à lista
        if(i==nplateleiras){
            prateleiras = (Prateleira*) realloc(prateleiras, (nplateleiras+1)*sizeof(Prateleira));
            prateleiras[i].tema = tema;
            prateleiras[i].listadelivros = novaListaDeLivros();
            transferirParaLista(&pilha, &prateleiras[i].listadelivros);
            nplateleiras++;
        }else{
            // Caso contrário, apenas acrescenta à quantidade de livros em um prateleira
            // No devido lugar
            transferirParaLista(&pilha, &prateleiras[i].listadelivros);
        }
    }

    FILE *arq = fopen(nomearq, "w");
    if(arq==NULL) return 1;
    for(int i=0; i<nplateleiras; i++){
        putchar('\n');

        if(prateleiras[i].tema==NULL){
            printf("Tema: OUTROS\n");
            fprintf(arq, "# OUTROS\n");

            ElementoListaLivros *pnt;
            pnt = prateleiras[i].listadelivros.inicio;

            while(pnt!=NULL){
                for(int i=0; i<pnt->quantidade; i++) fprintf(arq, "%s\n", pnt->livro.titulo);
                pnt = pnt->prox;
                fputc('\n', arq);
            }
        }else{
            printf("Tema: %s\n", prateleiras[i].tema);
            fprintf(arq, "# %s\n", prateleiras[i].tema);

            ElementoListaLivros *pnt;
            pnt = prateleiras[i].listadelivros.inicio;

            while(pnt!=NULL){
                for(int i=0; i<pnt->quantidade; i++) fprintf(arq, "%s\n", pnt->livro.titulo);
                pnt = pnt->prox;
                fputc('\n', arq);
            }

            free(prateleiras[i].tema);
        }

        mostrarLista(&prateleiras[i].listadelivros);
        finalizarListaDeLivros(&prateleiras[i].listadelivros);
    }
    fclose(arq);

    free(prateleiras);
    finalizarListaDeLivros(&lista);
    finalizarPilha(&pilha);

    return 0;
}

void mostrarMensagemDeAjuda(char* const exe_nome){
    printf("Modo de uso:\n");
    printf("$%s [-opção] nome_do_arquivo <quantidade>\n", exe_nome);
    printf("\nOPÇÕES:\n");
    printf("\t--aleatorio, -a: gera um arquivo de livros aleatorio com nome e quantidade solicitados pelo usuário\n");
    printf("\t--organizar, -o: organiza um arquivo de livros em ordem alfabética\n");
}
