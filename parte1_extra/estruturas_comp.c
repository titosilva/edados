// Implementação de prob_estruturas.h
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "estruturas_comp.h"
#include "arquivos_comp.h"

// MANIPULAÇÃO DOS TITULOS DE LIVROS ==========================================================
int ordemAlfabetica(char const *t1, char const *t2){
    int j, caso;
    int min = (strlen(t1)<strlen(t2))? strlen(t1) : strlen(t2);
    for(j=0; j<min; j++) if(toupper(t1[j])!=toupper(t2[j])) break;

    if( j==min && strlen(t1)==strlen(t2) ) return 0;
    else if( j==min && strlen(t1)<strlen(t2) ) return 1;
    else if( j==min && strlen(t2)<strlen(t1) ) return -1;

    for(int i=0; i<min; i++){
        if(toupper(t1[i])<toupper(t2[i])) return 1;
        else if(toupper(t1[i])>toupper(t2[i])) return -1;
    }
}

// LISTA ENCADEADA ============================================================================
ListaDeLivros novaListaDeLivros(){
    ListaDeLivros retorno;
    retorno.inicio = NULL;
    return retorno;
}

int buscarLivroEmLista(ListaDeLivros *lista, char const* titulo){
    // Ponteiro que percorre a lista
    ElementoListaLivros *pont = lista->inicio;
    // Posição em que o livro foi encontrado(ou onde ele deve ser inserido)
    int pos=0;

    while(pont!=NULL && ordemAlfabetica(titulo, pont->livro.titulo)==-1){
        pos++;
        pont=pont->prox;
    }

    if(pont==NULL || ordemAlfabetica(titulo, pont->livro.titulo)==1) return -1*pos-1;
    if(ordemAlfabetica(titulo, pont->livro.titulo)==0) return pos;
}

int adicionarLivroEmLista(ListaDeLivros *lista, char const* titulo){
    ElementoListaLivros *pnt = lista->inicio;
    int pos = buscarLivroEmLista(lista, titulo);

    if(pos<0){
        // Caso o titulo nao esteja na lista, cria um novo elemento para ele
        ElementoListaLivros *novo = (ElementoListaLivros*) malloc(sizeof(ElementoListaLivros));

        if(pos==-1){
            lista->inicio = novo;
            novo->livro.titulo = (char*) malloc(strlen(titulo)+1);
            strcpy(novo->livro.titulo, titulo);
            novo->prox = pnt;
            novo->quantidade = 1;
            return 0;
        }else{
            for(int i=0; i<-1*pos-2; i++) pnt=pnt->prox;
            novo->livro.titulo = (char*) malloc(strlen(titulo)+1);
            strcpy(novo->livro.titulo, titulo);
            novo->prox = pnt->prox;
            pnt->prox = novo;
            novo->quantidade = 1;
            return 0;
        }
    }else{
        // Caso o titulo ja esteja na lista, incrementa o campo quantidade e retorna 1
        for(int i=0; i<pos; i++) pnt=pnt->prox;
        pnt->quantidade++;
        return 1;
    }
}

void finalizarListaDeLivros(ListaDeLivros *lista){
    ElementoListaLivros *ant=NULL;
    ElementoListaLivros *pnt = lista->inicio;
    while(pnt!=NULL){
        ant = pnt;
        pnt = pnt->prox;
        free(ant->livro.titulo);
        free(ant);
    }
    
}

void mostrarLista(ListaDeLivros *lista){
    ElementoListaLivros *pnt = lista->inicio;
    while(pnt!=NULL){
        printf("%s\t(%d)\n", pnt->livro.titulo, pnt->quantidade);
        pnt=pnt->prox;
    }
}

// PILHA ======================================================================================
PilhaDeLivros novaPilhaDeLivros(){
    PilhaDeLivros retorno;
    retorno.topo = -1;
    return retorno;
}

PilhaDeLivros montarPilhaArq(char const* nomearq){
    ListaDeLivros auxiliar = novaListaDeLivros();
    PilhaDeLivros retorno = novaPilhaDeLivros();
    char *nomelivro;
    int nlivros;
    
    if((nlivros = quantidadeDeLivros(nomearq))<=0) return retorno;

    for(int i=nlivros-1; i>=0; i--){
        nomelivro = nomeDeLivro(nomearq, i);
        Livro novolivro;
        novolivro.titulo = nomelivro;
        AdicionarLivroEmPilha(&retorno, novolivro);
    }

    return retorno;
}

int AdicionarLivroEmPilha(PilhaDeLivros *pilha, Livro livro){
    if(pilha->topo==(MAXPILHA-1)) return 0;

    pilha->livros[pilha->topo+1] = livro;
    pilha->topo++;
    return 1;
}

void imprimirPilhaDeLivros(PilhaDeLivros *pilha){
    for(int i=pilha->topo; i>=0; i--) printf("%s\n", pilha->livros[i].titulo);
}

int transferirParaLista(PilhaDeLivros *pilha, ListaDeLivros *lista){
    if(pilha->topo==-1) return 0;
    adicionarLivroEmLista(lista, pilha->livros[pilha->topo].titulo);
    free(pilha->livros[pilha->topo].titulo);
    pilha->topo--;
}

void finalizarPilha(PilhaDeLivros *pilha){
    while(pilha->topo>-1){ 
        free(pilha->livros[pilha->topo].titulo);
        pilha->topo--;
    }
}
