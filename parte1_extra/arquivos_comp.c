#include "arquivos_comp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// LISTA DE GERACAO ALEATORIA ==========================================================
// Verifica livros.txt para saber quantos livros há na lista de geração aleatória
// Retorna -1 caso não seja encontrado o arquivo livros.txt
int quantidadeDeLivrosLT(){
    FILE *livrostxt = fopen(LIVROS_TXT, "r");

    if(livrostxt==NULL) return -1;

    int nlivros=0;
    char c;
    rewind(livrostxt);

    c=fgetc(livrostxt);
    while(!feof(livrostxt)){
        switch(c){
        case '\n':
            while(( c = fgetc(livrostxt) )=='\n' && !feof(livrostxt) );
            break;
        case '#':
            while(( c = fgetc(livrostxt) )!='\n' && !feof(livrostxt) );
            break;
        default:
            nlivros++;
            while(( c = fgetc(livrostxt) )!='\n' && !feof(livrostxt) );
        }
    }

    fclose(livrostxt);
    return nlivros;
}

// Seleciona um livro dentro do arquivo de acordo com a ordem em que aparecem no arquivo
// Exemplo: 0 seleciona o primeiro arquivo que aparecer e retorna seu nome
// 2 seleciona o terceiro arquivo que aparecer e retorna seu nome
// Obs.: a função usa calloc, então lembre de dar um free no resultado retornado
char *nomeDeLivroLT(unsigned int posicao){
    // Procura pelo livro na posição indicada
    FILE *livrostxt = fopen(LIVROS_TXT, "r");

    char c;
    c = fgetc(livrostxt);

    // Pula o começo do arquivo
    while(1){
        switch(c){
        case '#':
            while((c = fgetc(livrostxt))!='\n');
            break;
        case '\n':
            c = fgetc(livrostxt);
            break;
        }

        if( c!='\n' && c!='#' ) break;
    }
    
    for(int i=0; i<posicao; i++){
        // Pula até o final da linha
        while(( c = fgetc(livrostxt) )!='\n');

        // Procura próxima linha válida
        while(1){
            switch(c){
            case '#':
                while((c = fgetc(livrostxt))!='\n');
                break;
            case '\n':
                c = fgetc(livrostxt);
                break;
            }

            if( c!='\n' && c!='#' ) break;
        }
    }

    // Aloca memoria para o retorno
    int k=0;
    char *retorno = (char*) calloc(k+1, sizeof(char));
    retorno[0] = c;


    while(1){
        c = fgetc(livrostxt);
        if(c==' '){
            if(( c = fgetc(livrostxt) )==' ') break;
            else{
                k+=2;
                char *temp = (char*) calloc(k+1, sizeof(char));
                if(temp==NULL) return NULL;
                for(int i=0; i<k-1; i++) temp[i] = retorno[i];
                temp[k-1] = ' ';
                temp[k] = c;

                free(retorno);
                retorno = temp;
            }
        }else if(c=='\n'){
            break;
        }else{
            k++;
            char *temp = (char*) calloc(k+1, sizeof(char));
            if(temp==NULL) return NULL;
            for(int i=0; i<k; i++) temp[i] = retorno[i];
            temp[k] = c;


            free(retorno);
            retorno = temp;
        }
    }

    fclose(livrostxt);
    return retorno;
}

// FUNCOES PARA OUTROS ARQUIVOS =========================================================

int adicionarNovaLinha(char const *nomearq){
    FILE *arq = fopen(nomearq, "a");

    if(arq == NULL) return -1;

    fputc('\n', arq);
    fclose(arq);
}

int quantidadeDeLivros(char const *nomearq){
    // Implementação similar a funcao quantidadeDeLivrosLT

    FILE *arq = fopen(nomearq, "r");

    if(arq==NULL) return -1;

    int nlivros=0;
    char c;
    rewind(arq);

    c=fgetc(arq);
    while(!feof(arq)){
        switch(c){
        case '\n':
            while(!feof(arq) && ( c = fgetc(arq) )=='\n');
            break;
        case '#':
            while(!feof(arq) && ( c = fgetc(arq) )!='\n');
            break;
        default:
            nlivros++;
            while(!feof(arq) && ( c = fgetc(arq) )!='\n');
        }
    }

    fclose(arq);
    return nlivros;
}

char *nomeDeLivro(char const *nomearq, unsigned int posicao){
    // Procura pelo livro na posição indicada
    FILE *arq = fopen(nomearq, "r");

    char c;
    c = fgetc(arq);

    // Pula o começo do arquivo
    while(1){
        switch(c){
        case '#':
            while((c = fgetc(arq))!='\n');
            break;
        case '\n':
            c = fgetc(arq);
            break;
        }

        if( c!='\n' && c!='#' ) break;
    }
    
    for(int i=0; i<posicao; i++){
        // Pula até o final da linha
        while(( c = fgetc(arq) )!='\n');

        // Procura próxima linha válida
        while(1){
            switch(c){
            case '#':
                while((c = fgetc(arq))!='\n');
                break;
            case '\n':
                c = fgetc(arq);
                break;
            }

            if( c!='\n' && c!='#' ) break;
        }
    }

    // Aloca memoria para o retorno
    int k=0;
    char *retorno = (char*) calloc(k+1, sizeof(char));
    retorno[0] = c;


    while(1){
        c = fgetc(arq);
        if(c==' '){
            if(( c = fgetc(arq) )==' ') break;
            else{
                k+=2;
                char *temp = (char*) calloc(k+1, sizeof(char));
                if(temp==NULL) return NULL;
                for(int i=0; i<k-1; i++) temp[i] = retorno[i];
                temp[k-1] = ' ';
                temp[k] = c;

                free(retorno);
                retorno = temp;
            }
        }else if(c=='\n'){
            break;
        }else{
            k++;
            char *temp = (char*) calloc(k+1, sizeof(char));
            if(temp==NULL) return NULL;
            for(int i=0; i<k; i++) temp[i] = retorno[i];
            temp[k] = c;


            free(retorno);
            retorno = temp;
        }
    }

    fclose(arq);
    return retorno;
}

char *temaDeLivro(char const *nomelivro){
    FILE *arq = fopen(LIVROS_TXT, "r");
    if(arq==NULL) return NULL;
    int i, j;

    int nlivros = quantidadeDeLivrosLT();
    char *nome;
    for(i=0; i<nlivros; i++){
        nome = nomeDeLivroLT(i);
        
        // Verifica se os dois nomes são iguais
        for(j=0; j<strlen(nome); j++) if(toupper(nome[j])!=toupper(nomelivro[j])) break;

        // Verifica se o for anterior resultou em nomes iguais
        if(j==strlen(nome)) break;
    }

    // se chegar ate aqui, significa que o livro nao foi encontrado
    if(i==nlivros || feof(arq)) return NULL;
    else{
        // Procura pelo local onde o livro está
        char c;
        c = fgetc(arq);

        // Pula o começo do arquivo
        while(1){
            switch(c){
            case '#':
                while((c = fgetc(arq))!='\n');
                break;
            case '\n':
                c = fgetc(arq);
                break;
            }

            if( c!='\n' && c!='#' ) break;
        }
        
        for(j=0; j<i; j++){
            // Pula até o final da linha
            while(( c = fgetc(arq) )!='\n');

            // Procura próxima linha válida
            while(1){
                switch(c){
                case '#':
                    while((c = fgetc(arq))!='\n');
                    break;
                case '\n':
                    c = fgetc(arq);
                    break;
                }

                if( c!='\n' && c!='#' ) break;
            }
        }

        int k=0;
        char *retorno = (char*) calloc(k+1, sizeof(char));

        // Pula até o local na linha onde está o tema do livro
        for(j=0; j<2; j++){
            while(1){
                if((c = fgetc(arq)) == ' ') if(( c=fgetc(arq) ) == ' ') break;
                if(c=='\t') break;
            }
            while(c==' '||c=='\t') c=fgetc(arq);
        }

        retorno[0] = c;
        // Copia tema para retorno
        while(1){
            c = fgetc(arq);
            if(feof(arq)) break;
            if(c==' '){
                if(( c = fgetc(arq) )==' ') break;
                else{
                    k+=2;
                    char *temp = (char*) calloc(k+1, sizeof(char));
                    if(temp==NULL) return NULL;
                    for(int i=0; i<k-1; i++) temp[i] = retorno[i];
                    temp[k-1] = ' ';
                    temp[k] = c;

                    free(retorno);
                    retorno = temp;
                }
            }else if(c=='\n'){
                break;
            }else{
                k++;
                char *temp = (char*) calloc(k+1, sizeof(char));
                if(temp==NULL) return NULL;
                for(int i=0; i<k; i++) temp[i] = retorno[i];
                temp[k] = c;


                free(retorno);
                retorno = temp;
            }
        }

        fclose(arq);
        return retorno;

    }
}
