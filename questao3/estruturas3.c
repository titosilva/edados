#include <stdlib.h>
#include <stdio.h>
#include "estruturas3.h"
#include <time.h>

void clearscreen()
{
#ifdef __unix__
    system("clear");
#elif _WIN32
    system("cls");
#elif __linux__
    system("clear");
#endif
}

fila *inicializar(PILHA *pilhadupla)
{
    //inicializa a fila e a pilha juntas
    inicializarPilha(pilhadupla);
    fila *FILA = (fila *)malloc(sizeof(fila));

    //verifica se há elementos na fila. Se sim, torna o inicio e o fim = 0
    if (FILA != NULL)
    {
        FILA->inicio = NULL;
        FILA->fim = NULL;
    }   
    return FILA;
}

int qntd_elementos(fila *FILA)
{
    int contador = 0;
    ponteiro aux = FILA->inicio;

    //se a fila estiver vazia retorna 0
    if (FILA == NULL)
    {
        return 0;
    }

    //enquanto o auxiliar nao chegar no ponteiro que aponta para null, incrementa contador
    while (aux != NULL)
    {
        contador = contador + 1;
        aux = aux->prox;
    }
    return contador;
}

int mostrar_extra(fila *FILA)
{
    ponteiro aux = FILA->inicio;

    if (aux == NULL)
    {
        printf("NULL\n");
    }
    //enquanto nao chegar no fim da fila printa os elementos extras
    while (aux != NULL)
    {
        printf("%d\n", aux->reg.campo_extra);
        aux = aux->prox;
    }
}

int mostrar_elementos(fila *FILA)
{
    //ponteiro auxiliar para o inicio da fila
    ponteiro aux = FILA->inicio;

    if (aux == NULL)
    {
        printf("NULL\n");
    }

    //enquanto nao chegar no fim printa os elementos
    while (aux != NULL)
    {
        printf("%d\n", aux->reg.chave);
        aux = aux->prox;
    }
}

int inserirElemento(fila *FILA, int REGISTRO, int EXTRA)
{
    //a cada novo elemento, aloca memoria do tipo *elemento
    ponteiro novo = (ponteiro)malloc(sizeof(elemento));

    //atualiza os campos
    novo->reg.chave = REGISTRO;
    novo->reg.campo_extra = EXTRA;

    novo->prox = NULL;

    if (FILA->inicio == NULL)
    {
        FILA->inicio = novo;
    }
    else
    {
        FILA->fim->prox = novo;
    }

    FILA->fim = novo;
}

int removerElemento(fila *FILA, PILHA *pilhadupla)
{
    int a = 0;
    if (FILA->inicio == NULL)
    {
        printf("A fila está vazia!\n");
    }

    else
    {

        ponteiro erase = FILA->inicio;
        FILA->inicio = FILA->inicio->prox;

        if (FILA->inicio == NULL)
        {
            FILA->fim == NULL;
        }
        //quando um elemento é excluido da fila, chama-se a função de inserir na pilha
        inserirPilha(pilhadupla, erase, FILA);
        free(erase);
    }
}

void reinicializarFila(fila *FILA, PILHA *pilhadupla)
{
    ponteiro aux = FILA->inicio;

    while (FILA->inicio != NULL)
    {
        aux = FILA->inicio;
        FILA->inicio = FILA->inicio->prox;
        free(aux);
    }
    FILA->inicio == NULL;
    FILA->fim == NULL;
    free(FILA);

    //ao reinicializar a pilha, inicializa-se a pilha tambem
    FILA = inicializar(pilhadupla);
}

/*--------------------------------------------------------------------------------------*/
/*PILHA DUPLA*/

int inicializarPilha(PILHA *pilhadupla)
{
    //como a pilha é dupla, há dois topos, que iniciam em -1 e size
    pilhadupla->topoA = -1;
    pilhadupla->topoB = SIZE;
    return 1;
}
int pilhavazia(PILHA *pilhadupla)
{

    if (pilhadupla->topoA == -1 && pilhadupla->topoB == SIZE)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
int contarelementos(PILHA *pilhadupla)
{
    return (pilhadupla->topoA + 1) + SIZE - pilhadupla->topoB;
}

int contPilhaA(PILHA *pilhadupla)
{
    return pilhadupla->topoA + 1;
}

int contPilhaB(PILHA *pilhadupla)
{
    return SIZE - pilhadupla->topoB;
}

int inserirPilha(PILHA *pilhadupla, ponteiro elemento, fila *FILA)
{
    int opcao = 0;
    if (pilhadupla->topoA + 1 == pilhadupla->topoB)
    {
        //menu de interação, caso a pilha esteja cheia
        while (opcao != 1 && opcao != 2 && opcao != 3){
            printf("A pilha está cheia! Você pode optar por:\n1-> Reinicializar pilha e continuar\n2->Deslocar elementos da pilha para a fila(com campo extra aleatorio)\n3->Ignorar próximas exclusões\n");
            scanf("%d", &opcao);
            while(getchar()!='\n');
        }
        printf("opcao: %d\n", opcao);
        switch (opcao)
        {
        case 1:
            inicializarPilha(pilhadupla);
            break;
        case 2:
        //função rand de 0 - 10, para definir um elemento extra aleatório para a fila
            srand(time(NULL));
            inserirElemento(FILA, pilhadupla->topoB, rand()%11);
            pilhadupla->topoB = pilhadupla->topoB+1;
            break;
        case 3:
            break;
        default:
            if (opcao != 1 && opcao != 2 && opcao != 3)
            {
                clearscreen();
                printf("Opção inválida!\n");
            }
            break;
        }
    }
    //se a pilha nao estiver cheia, insere elementos, de acordo com o campo extra
    else 
    if (elemento->reg.campo_extra<=5)
    {
        pilhadupla->topoA = pilhadupla->topoA + 1;
        pilhadupla->pilha[pilhadupla->topoA] = elemento;
    }
    else
    {
        pilhadupla->topoB = pilhadupla->topoB - 1;
        pilhadupla->pilha[pilhadupla->topoB] = elemento;
    }
}
