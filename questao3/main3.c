#include <stdlib.h>
#include "estruturas3.h"
#include <stdio.h>

int main()
{
    //inicializando estruturas
    fila *FILA;
    PILHA pilhad;
    inicializarPilha(&pilhad);
    PILHA *pilhadupla = &pilhad;
    int REGISTRO = 0;
    int EXTRA = 0;

    int opcao = 0, aux = 0, a = 0;
    int visualizar = 0;
    // Verifica se ocorreu uma inicializacao
    int inicializado = 0;

    //loop para continuar no menu até que a parada seja solicitada pelo usuário
    while (aux != -1)
    {
        printf("O que você deseja fazer?\n1->Inicializar a fila\n2->Retornar qtd de elementos válidos\n3->imprimir campos extras\n4->Imprimir todas as chaves\n5->Inserir um novo elemento\n6->Eliminar um elemento\n7->Reinicializar a fila\n8->Entrar na janela de manipulação de pilha\n-1->Sair\n");
        scanf("%d", &aux);

        // Esvazia buffer
        while (getchar() != '\n')
            ;

        switch (aux)
        {
        case 1:
            if(!inicializado){
                FILA = inicializar(pilhadupla); //passo o "pilhadupla por parametro para inicializar a pilha junto com a fila"
                clearscreen();
                if (FILA->inicio == NULL)
                {
                    printf("Fila inicializada!\n\n");
                }
                inicializado = 1;
            }else{
                clearscreen();
                printf("As estruturas ja foram inicializadas\n");
            }

            break;
        case 2:
            if(inicializado){
                //visualizar recebe o return com qtd de elementos da fila e printa
                visualizar = qntd_elementos(FILA);
                clearscreen();
                printf("A quantidade de elementos presentes é %d\n\n", visualizar);
            }else{
                clearscreen();
                printf("Inicialize as estruturas antes de realizar outras operacoes\n");
            }

            break;
        case 3:
            if(inicializado){
                //aqui mostramos os elementos extras da fila
                clearscreen();
                mostrar_extra(FILA);
                break;
            }else{
                clearscreen();
                printf("Inicialize as estruturas antes de realizar outras operacoes\n");
            }
        case 4:
            if(inicializado){
                //aqui mostramos os elementos da fila
                clearscreen();
                mostrar_elementos(FILA);
            }else{
                clearscreen();
                printf("Inicialize as estruturas antes de realizar outras operacoes\n");
            }

            break;
        case 5:
            if(inicializado){
                //recebemos um registro e um elemento extra a ser adicionado
                printf("digite o elemento que voce deseja inserir: \n");
                scanf("%d", &REGISTRO);
                printf("digite um elemento extra (0 - 10): \n");
                scanf("%d", &EXTRA);
                while (EXTRA > 10 || EXTRA < 0)
                {
                    printf("Elemento inválido. Digite um elemento extra (0 - 10): \n");
                    // Esvazia o buffer do teclado
                    while (getchar() != '\n')
                        ;
                    scanf("%d", &EXTRA);
                }

                inserirElemento(FILA, REGISTRO, EXTRA);
                clearscreen();
            }else{
                clearscreen();
                printf("Inicialize as estruturas antes de realizar outras operacoes\n");
            }

            break;
        case 6:
            if(inicializado){
                clearscreen();
                removerElemento(FILA, pilhadupla);
            }else{
                clearscreen();
                printf("Inicialize as estruturas antes de realizar outras operacoes\n");
            }

            break;
        case 7:
            if(inicializado){
                reinicializarFila(FILA, pilhadupla);
                clearscreen();
                if (FILA->inicio == NULL)
                {
                    printf("Fila reinicializada!\n\n");
                }
            }else{
                clearscreen();
                printf("Inicialize as estruturas antes de realizar outras operacoes\n");
            }

            break;
        case 8:
            if(inicializado){
                clearscreen();
                a = 0;
                while (a != -1)
                {
                    printf("Selecione uma opção:\n1->Checar se a pilha está vazia\n2->Contar todos os elementos da pilha\n3->Contar elementos do topo 'A'\n4->Contar elementos do topo 'B'\n-1->Sair\n");
                    scanf("%d", &a);

                    while (getchar() != '\n')
                        ;
                    switch (a)
                    {
                    case 1:
                        visualizar = pilhavazia(pilhadupla);
                        //a funçao recebe um retorno. se estiver vazia retorna 1.
                        clearscreen();

                        if (visualizar == 1)
                        {
                            printf("A pilha está vazia\n");
                        }
                        else
                        {
                            printf("A pilha não está vazia\n");
                        }

                        break;

                    case 2:

                        visualizar = contarelementos(pilhadupla);
                        clearscreen();
                        printf("Há %d elementos na pilha\n", visualizar);

                        break;

                    case 3:
                        visualizar = contPilhaA(pilhadupla);
                        clearscreen();
                        printf("Há %d elementos com campo extra menor ou igual a 5\n", visualizar);
                        break;

                    case 4:
                        visualizar = contPilhaB(pilhadupla);
                        clearscreen();
                        printf("Há %d elementos com campo extra maior que 5\n", visualizar);
                        break;

                    default:
                        if (a != -1)
                        {
                            clearscreen();
                            printf("Opção inválida!\n");
                        }

                        break;
                    }
                }
            }else{
                clearscreen();
                printf("Inicialize as estruturas antes de realizar outras operacoes\n");
            }
            break;
        default:
            if (aux != -1)
            {
                clearscreen();
                printf("Opção inválida!\n");
            }

            break;
        }
    }

    return 0;
}
