#ifndef ESTRUTURAS_QUESTAO_3_H
#define ESTRUTURAS_QUESTAO_3_H

// Tamanho da pilha/
#define SIZE 50
typedef struct
{
    int chave;
    int campo_extra;
} registro;

typedef struct elemento
{
    registro reg;
    struct elemento *prox;
} elemento, *ponteiro;

typedef struct
{
    ponteiro inicio;
    ponteiro fim;
} fila;

typedef struct
{
    ponteiro pilha[SIZE];
    int topoA;
    int topoB;

} PILHA;

void clearscreen();//função de interação com o terminal criada por nós

fila *inicializar(PILHA *pilhadupla);

int qntd_elementos(fila *FILA);

int mostrar_elementos(fila *FILA);

int mostrar_extra(fila *FILA);

int inserirElemento(fila *FILA, int REGISTRO, int EXTRA);

int removerElemento(fila *FILA, PILHA *pilhadupla);

void reinicializarFila(fila *FILA, PILHA *pilhadupla);

/*--------------------------------------------------------------------------------------------------------*/

/*PILHA DUPLA ESTÁTICA*/


int inicializarPilha(PILHA *pilhadupla);

int pilhavazia(PILHA *pilhadupla);

int contarelementos(PILHA *pilhadupla);

void reinicializarpilha(PILHA *pilhadupla);

int contPilhaA(PILHA *pilhadupla);

int contPilhaB(PILHA *pilhadupla);

int inserirPilha(PILHA *pilhadupla, ponteiro elemento, fila *FILA);

#endif
