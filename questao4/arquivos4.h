#ifndef ARQUIVOS_4_H
#define ARQUIVOS_4_H

// FUNCOES GENERICAS ======================================================================================
// Retorna a quantidade de "objetos" presentes em um arquivo
int quantidadeDeObjetos(char const *nomearq);
// Retorna uma determinada informação sobre um objeto
// Os argumentos sao o nome do arquivo, o nome do objeto e a posição, dentro da linha,
// em que a informação está localizada.
char *infoObjeto(char const *nomearq, char const *nomeobj, int pos);
// Retorna o nome do objeto situado na linha de numero pos(contando a partir do 0)
// Retorna NULL caso nao seja possivel encontrar um objeto na linha indicada
char *nomeDeObjeto(char const *nomearq, unsigned int pos);

// ARQUIVO DAS TRANSPORTADORAS ============================================================================
// Retorna a quantidade de transportadoras presentes no arquivo transportadoras.txt
int quantidadeDeTransportadoras();
// Retorna o nome da transportadora na posição indicada do arquivo
char *nomeDeTransportadora(unsigned int posicao);
// Retorna o fator de preferencia da tranportadora indicada
int fatorDePreferenciaTransportadora(char const *nometransp);
// Retorna a velocidade da transportadora indicada
int velocidadeTransportadora(char const *nometransp);

// ARQUIVO DOS LIVROS =====================================================================================
// Retorna a quantidade de livros presentes no arquivo
int quantidadeDeLivros();
// Retorna o nome do livro encontrado na posicao indicada
char *nomeDeLivro(unsigned int posicao);
// Retorna o estoque inicial de determinado livro
int estoqueDeLivro(char const *nomelivro);
// Retorna o fator de escolha de determinado livro
int fatorDeEscolhaDeLivro(char const *nomelivro);

#endif