#include "arquivos4.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
    Este arquivo contem as implementações utilizadas na questão 4 para manusear os arquivos transportadoras.txt
    e livros.txt, utilizados na dinamica do programa.
    Esses arquivos seguem um padrão: linhas em branco são ignoradas, assim como linhas iniciadas com '#'.
*/

// FUNCOES GENERICAS =======================================================================================
int quantidadeDeObjetos(char const *nomearq){
    FILE *arq = fopen(nomearq, "r");

    if(arq==NULL) return -1;

    int ntransp=0;
    char c;
    rewind(arq);

    c=fgetc(arq);
    while(!feof(arq)){
        switch(c){
        // Pula linhas vazias
        case '\n':
            while(( c = fgetc(arq) )=='\n' && !feof(arq) );
            break;
        case '#':
        // Pula linhas de comentário
            while(( c = fgetc(arq) )!='\n' && !feof(arq) );
            break;
        default:
        // Se não for nenhum caso anterior, a linha contem informações sobre algum objeto
            ntransp++;
            while(( c = fgetc(arq) )!='\n' && !feof(arq) );
        }
    }

    fclose(arq);
    return ntransp;
}

char *nomeDeObjeto(char const *nomearq, unsigned int posicao){
    // Se a posicao passada é maior ou igual a quantidade de livros,
    // Não começa a busca
    if(posicao >= quantidadeDeObjetos(nomearq)) return NULL;

    // Procura pela Objeto na posição indicada
    FILE *arq = fopen(nomearq, "r");

    char c;
    c = fgetc(arq);

    // Pula o começo do arquivo
    while(1){
        switch(c){
        case '#':
            while((c = fgetc(arq))!='\n');
            break;
        case '\n':
            c = fgetc(arq);
            break;
        }

        if( c!='\n' && c!='#' ) break;
    }
    
    for(int i=0; i<posicao; i++){
        // Pula até o final da linha
        while(( c = fgetc(arq) )!='\n');

        // Procura próxima linha válida
        while(1){
            switch(c){
            case '#':
                while((c = fgetc(arq))!='\n');
                break;
            case '\n':
                c = fgetc(arq);
                break;
            }

            if( c!='\n' && c!='#' ) break;
        }
    }

    // Aloca memoria para o retorno
    int k=0;
    char *retorno = (char*) calloc(k+1, sizeof(char));

    retorno[0] = c;
    // Copia fator de preferencia para retorno
    while(1){
        c = fgetc(arq);
        if(feof(arq)) break;
        // Caso seja detectada uma barra de espaço, é necessário ver se não há duas barras de espaço
        // em sequência. Se houver, significa que a informação acaba ali
        if(c==' '){
            if(( c = fgetc(arq) )==' ') break;
            else{
                // Caso não haja duas barras de espaço em sequencia, adiciona ao retorno tanto
                // a barra de espaço quanto o caractere que vem logo depois
                k+=2;
                char *temp = (char*) calloc(k+1, sizeof(char));
                // erro de alocação diâmica
                if(temp==NULL) return NULL;
                for(int i=0; i<k-1; i++) temp[i] = retorno[i];
                temp[k-1] = ' ';
                temp[k] = c;

                free(retorno);
                retorno = temp;
            }
        }else if(c=='\n'){
            // Se houver uma nova linha, significa que é o fim da informação
            break;
        }else{
            // Copia caractere para o retorno
            k++;
            char *temp = (char*) calloc(k+1, sizeof(char));
            // erro de alocação diâmica
            if(temp==NULL) return NULL;
            for(int i=0; i<k; i++) temp[i] = retorno[i];
            temp[k] = c;


            free(retorno);
            retorno = temp;
        }
    }

    fclose(arq);
    return retorno;
}

char *infoObjeto(char const *nomearq, char const *nomeobj, int pos){
    // Tenta abrir o arquivo. Caso não seja possível, retorna NULL
    FILE *arq = fopen(nomearq, "r");
    if(arq==NULL) return NULL; 
    int i, j;

    // Obtem a quantidade de objetos presentes no arquivo
    int nobj = quantidadeDeObjetos(nomearq);

    char *nome;
    // Procura, dentro do arquivo, o objeto com nome selecionado pelo usuario
    for(i=0; i<nobj; i++){
        // Seleciona um objeto dentro do arquivo
        nome = nomeDeObjeto(nomearq, i);
        
        // Verifica se os dois nomes são iguais
        for(j=0; j<strlen(nome); j++) if(toupper(nome[j])!=toupper(nomeobj[j])) break;

        // Verifica se o loop for anterior resultou em nomes iguais
        if(j==strlen(nome)) break;
    }

    // Se o objeto não for encontrado, retorna NULL
    if(i==nobj) return NULL;
    else{
        // Procura pelo local onde o Objeto está
        char c;
        c = fgetc(arq);

        // Pula o começo do arquivo
        while(1){
            switch(c){
            // Caso seja encontrado um '#', essa é uma linha de comentário, portanto deve ser ignorada
            case '#':
                while((c = fgetc(arq))!='\n');
                break;
            // Caso a proxima linha seja um linha vazia, só é necessário repetir o mesmo processo
            // para a próxima linha
            case '\n':
                c = fgetc(arq);
                break;
            }

            // Caso o próximo caractere não seja uma linha vazia nem uma linha de comentario,
            // Para o loop
            if( c!='\n' && c!='#' ) break;
        }
        
        // Repete a ideia de pular linhas de comentario e novas linhas
        // Ignora linhas que nao sao desejadas
        // Até chegar no objeto desejado(indicado pela linha valida de numero i)
        for(j=0; j<i; j++){
            // Pula até o final da linha
            while(( c = fgetc(arq) )!='\n');

            // Procura próxima linha válida
            while(1){
                switch(c){
                case '#':
                    while((c = fgetc(arq))!='\n');
                    break;
                case '\n':
                    c = fgetc(arq);
                    break;
                }

                if( c!='\n' && c!='#' ) break;
            }
        }

        // Prepara para retornar o valor obtido
        int k=0;
        char *retorno = (char*) calloc(k+1, sizeof(char));

        // Pula até o local na linha onde está o fator de preferencia da Objeto
        for(j=0; j<pos; j++){
            // Enquanto não é encontrado um '\t' ou duas barras de espaço em seguida
            // ainda é uma mesma informação. O algoritmo deve pular as informações na linha
            // até chegar à informação desejada, que se encontra na posição pos na linha.
            while(1){
                if((c = fgetc(arq)) == ' ') if(( c=fgetc(arq) ) == ' ') break;
                if(c=='\t') break;
            }
            while(c==' '||c=='\t') c=fgetc(arq);
        }

        retorno[0] = c;
        // Copia fator de preferencia para retorno
        while(1){
            c = fgetc(arq);
            if(feof(arq)) break;
            // Caso seja detectada uma barra de espaço, é necessário ver se não há duas barras de espaço
            // em sequência. Se houver, significa que a informação acaba ali
            if(c==' '){
                if(( c = fgetc(arq) )==' ') break;
                else{
                    // Caso não haja duas barras de espaço em sequencia, adiciona ao retorno tanto
                    // a barra de espaço quanto o caractere que vem logo depois
                    k+=2;
                    char *temp = (char*) calloc(k+1, sizeof(char));
                    // erro de alocação diâmica
                    if(temp==NULL) return NULL;
                    for(int i=0; i<k-1; i++) temp[i] = retorno[i];
                    temp[k-1] = ' ';
                    temp[k] = c;

                    free(retorno);
                    retorno = temp;
                }
            }else if(c=='\n'){
                // Se houver uma nova linha, significa que é o fim da informação
                break;
            }else{
                // Copia caractere para o retorno
                k++;
                char *temp = (char*) calloc(k+1, sizeof(char));
                // erro de alocação diâmica
                if(temp==NULL) return NULL;
                for(int i=0; i<k; i++) temp[i] = retorno[i];
                temp[k] = c;


                free(retorno);
                retorno = temp;
            }
        }

        fclose(arq);
        return retorno;

    }
}

// ARQUIVO DAS TRANSPORTADORAS =============================================================================
#define TRANSPORTADORASTXT "./info/transportadoras.txt"

int quantidadeDeTransportadoras(){
    return quantidadeDeObjetos(TRANSPORTADORASTXT);
}

char *nomeDeTransportadora(unsigned int posicao){
    return nomeDeObjeto(TRANSPORTADORASTXT, posicao);
}

int fatorDePreferenciaTransportadora(char const *nometransp){
    char *info = infoObjeto(TRANSPORTADORASTXT, nometransp, 2);
    int ret = atoi(info);
    free(info);
    return ret;
}

int velocidadeTransportadora(char const *nometransp){
    char *info = infoObjeto(TRANSPORTADORASTXT, nometransp, 1);
    int ret = atoi(info);
    free(info);
    return ret;
}

// ARQUIVO DOS LIVROS + ESTOQUE INICIAL =====================================================================
#define ESTOQUE "./info/livros.txt"

int quantidadeDeLivros(){
    return quantidadeDeObjetos(ESTOQUE);
}

char *nomeDeLivro(unsigned int posicao){
    return nomeDeObjeto(ESTOQUE, posicao);
}


int estoqueDeLivro(char const *nomelivro){
    char *info = infoObjeto(ESTOQUE, nomelivro, 1);
    int ret = atoi(info);
    free(info);
    return ret;
}

int fatorDeEscolhaDeLivro(char const *nomelivro){
    char *info = infoObjeto(ESTOQUE, nomelivro, 2);
    int ret = atoi(info);
    free(info);
    return ret;
}