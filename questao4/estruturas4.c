#include "estruturas4.h"
#include <stdlib.h>
#include <stdio.h>

FilaDeEntrega novaFilaDeEntrega(){
    FilaDeEntrega retorno;
    retorno.inicio = 0;
    retorno.quantidade = 0;

    return retorno;
}

int adicionarPedidoFila(FilaDeEntrega *fila, Livro livro, int cep){
    // Verifica se a fila já está cheia
    if(fila->quantidade==FILAMAX) return 1;
    // Se não estiver, adiciona à fila
    else{
        // Cria pedido a ser adicionado
        Pedido pedido;
        pedido.destinatario = cep;
        pedido.livro = livro;
        
        // Como o vetor é estático, fazemos (fila->inicio + fila->quantidade)%FILAMAX
        // Desse modo, evitamos que o computador tente gravar o pedido em um lugar fora
        // do vetor
        fila->pedidos[(fila->inicio + fila->quantidade)%FILAMAX] = pedido;
        fila->quantidade++;

        return 0;
    }
}

Pedido removerPedidoFila(FilaDeEntrega *fila){
    // Gera pedido a ser retornado
    Pedido retorno;
    retorno.destinatario = 0;
    retorno.livro.titulo = NULL;

    // Caso a fila esteja vazia, retorna um pedido nulo
    if(fila->quantidade==0) return retorno;
    // Caso contrário, remove pedido do inicio da fila, 
    // seguindo o modelo FIFO
    else{
        retorno = fila->pedidos[fila->inicio];
        fila->inicio = (fila->inicio+1)%FILAMAX;
        fila->quantidade--;
    }
    return retorno;
}

int quantidadeFila(FilaDeEntrega *fila){
    return fila->quantidade;
}