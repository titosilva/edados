#ifndef ESTRUTURAS_4_H
#define ESTRUTURAS_4_H

// PEDIDO ========================================================================================

typedef struct livro_str{
    char *titulo;
}Livro;

typedef struct pedidostr{
    // CEP do destinatario
    int destinatario;
    // Livro encomendado
    Livro livro;
} Pedido;


// FILA ESTATICA =================================================================================

// Tamanho maximo da fila
#define FILAMAX 30

typedef struct filaest_str{
    // Pedidos
    Pedido pedidos[FILAMAX];
    // Quantidade de pedidos realizados
    int quantidade;
    // Inicio da fila
    int inicio;
} FilaDeEntrega;

// Retorna fila de entrega com 0 pedidos
FilaDeEntrega novaFilaDeEntrega();

// Adiciona um pedido à fila
// Retorna 0 se houver espaço
// Retorna 1 se não houver espaço
int adicionarPedidoFila(FilaDeEntrega *fila, Livro livro, int cep);

// Remove um pedido da fila
// E retorna o elemento que foi removido
// Caso nao haja elementos restantes na fila, 
// Retorna um pedido com CEP = 0
Pedido removerPedidoFila(FilaDeEntrega *fila);

// Retorna a quantidade de elementos na fila
int quantidadeFila(FilaDeEntrega *fila);

#endif