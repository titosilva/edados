#include "estruturas4.h"
#include "arquivos4.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

// Limpa a tela usando as funções de linha de comandos
void limparTela();
// Imprime o estoque de cada livro na tela
void mostrarEstoque(int nlivros, char **titulos, int *estoque);
// Imprime as transportadoras, com suas respectivas filas de entrega
void mostrarFilas(int ntransp, char **transportadoras, FilaDeEntrega *fila);
// Retorna um numero correspondente a um objeto selecionado aleatoriamente
// Leva em conta os fatores de preferencia
// Toma como argumentos a quantidade de objetos (no nosso caso, o total de livros/transportadoras)
// Tambem toma o vetor de fatores de preferencia
int selecionarFP(int nobj, int *fatordp);

// Quantidade de dias que serão exibidos
#define NDIAS 15
// Intervalo entre encomendas
#define DIAENCOMENDA 30
// Quantidade de livros encomendados por vez
#define TAMENCOMENDA 5
// Quantidades maxima e minima de livros vendidos por dia
#define VENDASMAX 10
#define VENDASMIN 0
// Quantidade maxima que uma tranportadora pega por vez
#define MAXTRANSP 5
// Intervalo entre um dia e outro (em segungos)
#define INTERVALO 2
// Quantidade que sera utilizada para correspondencia entre livros e '#'
// na parte de mostrar os estoques na tela
#define MODULOLIVROS 10
// Quantidade que sera utilizada para correspondencia entre pedidos na fila e '@'
#define MODULOPEDIDOS 1

int main(int argc, char const *argv[]){
    // Inicaliza com uma seed para geração de numeros pseudo-aleatorios
    srand(time(NULL));

    // Obter as quantidades de livros e transportadoras
    int nlivros, ntransp;
    nlivros = quantidadeDeLivros();
    ntransp = quantidadeDeTransportadoras();

    // Vetores
    char **titulos = (char**) calloc(nlivros, sizeof(char*)); // titulos de livros
    int *estoque = (int*) calloc(nlivros, sizeof(int));
    int *fatordplivros = (int*) calloc(nlivros, sizeof(int));

    char **transportadoras = (char**) calloc(ntransp, sizeof(char*)); // nomes das transportadoras
    int *velocidades = (int*) calloc(ntransp, sizeof(int));
    int *fatordptransp = (int*) calloc(ntransp, sizeof(int));

    // Array de filas de entrega
    // Cada empresa de entrega possuirá sua própria fila de encomendas
    FilaDeEntrega *fila = (FilaDeEntrega*) calloc(ntransp, sizeof(FilaDeEntrega)); 

    // Copia dados obtidos de livros.txt
    for(int i=0; i<nlivros; i++) titulos[i] = nomeDeLivro(i);
    for(int i=0; i<nlivros; i++) estoque[i] = estoqueDeLivro(titulos[i]);
    for(int i=0; i<nlivros; i++) fatordplivros[i] = fatorDeEscolhaDeLivro(titulos[i]);

    // Copia dados obtidos de transportadoras.txt
    for(int i=0; i<ntransp; i++) transportadoras[i] = nomeDeTransportadora(i);
    for(int i=0; i<ntransp; i++) velocidades[i] = velocidadeTransportadora(transportadoras[i]);
    for(int i=0; i<ntransp; i++) fatordptransp[i] = fatorDePreferenciaTransportadora(transportadoras[i]);
    // Inicializa as filas de entrega
    for(int i=0; i<ntransp; i++) fila[i] = novaFilaDeEntrega();
    
    for(int i=0; i<NDIAS; i++){
        int r;
        // Imprime informações na tela
        limparTela();
        printf("\033[32;1mDIA %d\n\n\033[0m", i+1);
        mostrarEstoque(nlivros, titulos, estoque);
        putchar('\n');
        mostrarFilas(ntransp, transportadoras, fila);

        // Determina informacoes para o proximo dia
        // Dias de encomenda de novos livros
        if((i+1)%DIAENCOMENDA==0){
            for(int j=0; j<TAMENCOMENDA; j++){
                // Seleciona livro aleatoriamente e adiciona ao estoque
                int sel = selecionarFP(nlivros, fatordplivros);
                estoque[sel]++;
            }
        }

        putchar('\n');
        // Transportadoras
        printf("\033[32;4mPEDIDOS ENTREGADOS NO DIA\n");
        for(int j=0; j<ntransp; j++){
            // Para cada transportadora, calcula se ela recebeu um pedido ou não
            /* O cálculo se dá da seguinte maneira:
                Primeiramente, é obtido do arquivo info/transportadoras.txt o valor
                velocidade da transportadora em questão. Usando rand(), é gerado um
                numero aleatorio de 0 a 100. O numero gerado e a velocidade sao 
                comparados e, se o primeiro for menor que o ultimo, significa que 
                naquele dia a empresa foi buscar encomendas. Logo, quanto maior for 
                o fator velocidade de uma transportadora, maior é a chance de que, em 
                determinado dia, ela busque encomendas
            */
            if(rand()%100 < velocidadeTransportadora(transportadoras[j])){
                // Retira o maximo de pedidos possivel(MAXTRANSP)
                for(int k=1; k<=MAXTRANSP; k++){
                    Pedido pedido = removerPedidoFila(&fila[j]);
                    if(pedido.livro.titulo!=NULL){
                        printf("\033[31;4;1m%.13s", transportadoras[j]);
                        if(strlen(transportadoras[j])>13) putchar('~');
                        printf("\t(CEP: %d)\t->\t%.13s", pedido.destinatario, pedido.livro.titulo);
                        printf("\033[0m\n");
                    }
                }
            }
        }

        putchar('\n');
        // Gera pedidos aleatorios
        printf("\033[32;4mNOVOS PEDIDOS\n");
        r = rand()%(VENDASMAX-VENDASMIN) + VENDASMIN;
        for(int j=0; j<r; j++){
            // Seleciona uma transportadora
            int selt = selecionarFP(ntransp, fatordptransp);
            // Seleciona um livro
            int sell = selecionarFP(nlivros, fatordplivros);

            // Salva o novo livro com o titulo selecionado
            Livro livro;
            livro.titulo = titulos[sell];

            // Se ainda houver livros como este no estoque, o pedido é efetuado
            if(estoque[sell]>0){
                // Retirando livro do estoque
                estoque[sell]--;
                // Adicionando à fila de entrega da empresa selecionada
                adicionarPedidoFila(&fila[selt], livro, rand()%10000000);
                // Imprimindo na tela o novo pedido
                printf("\033[36;4;1m%.13s", livro.titulo);
                printf("\t->\t%.13s", transportadoras[selt]);
                if(strlen(transportadoras[selt])>13) putchar('~');
                printf("\033[0m\n");
            }else{
                // Caso não haja estoque o suficiente, imprime na tela
                // um 'X', que indica que o pedido não pode ser realizado po falta de estoque
                printf("\033[36;4;1m%.13s", livro.titulo);
                if(strlen(transportadoras[selt])>13) putchar('~');
                printf("\tX");
                printf("\033[0m\n");
            }
        }

        // Para a execução por um momento, para ser possível a visualização
        // dos resultados
        sleep(INTERVALO);
    }

    // Libera a memória que havia sido alocada dinamicamente no início da execução
    for(int i=0; i<ntransp; i++) free(transportadoras[i]);
    for(int i=0; i<nlivros; i++) free(titulos[i]);
    free(titulos);
    free(transportadoras);
    free(velocidades);
    free(fatordplivros);
    free(fatordptransp);
    free(estoque);
    

    return 0;
}

void limparTela(){
    #ifdef __unix__
        system("clear");
    #elif _WIN32
        system("cls");
    #endif
}

void mostrarEstoque(int nlivros, char **titulos, int *estoque){
    printf("LIVRO\t\tESTOQUE\n");
    for(int i=0; i<nlivros; i++){
        printf("\033[36;4;1m%.13s", titulos[i]);
        printf("\033[0m");
        if(strlen(titulos[i])>13) putchar('~');
        putchar('\t');
        for(int j=0; j<estoque[i]; j+=MODULOLIVROS){
            if(j<5*MODULOLIVROS){
                printf("\033[31;1m#");
            }else if(j<10*MODULOLIVROS){
                printf("\033[33;1m#");
            }else{
                printf("\033[32;1m#");
            }
        }
        for(int j=0; j<estoque[i]%MODULOLIVROS; j++) putchar('.');
        putchar('\n');
    }
    printf("\033[0m");
}

void mostrarFilas(int ntransp, char **transportadoras, FilaDeEntrega *fila){
    printf("FILAS DE ENTREGA\n");
    for(int i=0; i<ntransp; i++){
        printf("\033[36;4;1m%.13s", transportadoras[i]);
        printf("\033[0m");
        if(strlen(transportadoras[i])>13) putchar('~');
        putchar('\t');
        for(int j=0; j<quantidadeFila(&fila[i]); j+=MODULOPEDIDOS){
            if(j<5*MODULOPEDIDOS){
                printf("\033[32;1m@");
            }else if(j<10*MODULOPEDIDOS){
                printf("\033[33;1m@");
            }else{
                printf("\033[31;1m@");
            }
        }
        for(int j=0; j<quantidadeFila(&fila[i])%MODULOPEDIDOS; j++) putchar('.');
        if(quantidadeFila(&fila[i])==FILAMAX) putchar('x');
        putchar('\n');
    }
    printf("\033[0m");
}

int selecionarFP(int nobj, int *fatordp){
    // Essa função seleciona um livro ou uma transportadora aleatoriamente
    // Retorna o numero correspondente à posição, no vetor, do objeto selecionado
    /* Essa função trabalha da seguinte maneira:
        No começo da execução, são alocados os vetores com os fatores de preferência.
        Esses vetores contém inteiros, e a ideia é que, quanto maior for esse valor,
        mais provável é que o objeto(livro ou transportadora) correspondente seja escolhido.
        Para isso, o algoritmo usa intervalos. Digamos, por exemplo, que os fatores eram
        LivroA 30
        LivroB 50
        LivroC 20
        O algoritmo calcula a soma desses fatores(que resulta em 100). Então, ele aplica um
        rand()%soma, que, no nosso exemplo, seria um numero entre 0 e 100. Com esse resultado,
        ele analisa qual intervalo foi selecionado: se o numero for menor que 30, o livro A foi
        selecionado. Se o numero for maior que 30 e menor que 80 = 30+50, então o livro B foi 
        selecionado. Se o numero for maior que 80 = 30+50 e menor que 100 = 30+50+20, entao o 
        livro C foi selecionado. Dessa maneira, quanto maior for o fator de preferência de um
        livro, mais provável é que seja selecionado.
    */

    // Calcula soma dos fatores de preferencia
    int somafp = 0, i;

    for(i=0; i<nobj; i++) somafp += fatordp[i];

    // Calcula numero aleatorio e reconhece qual livro corresponde à faixa
    // a qual esse numero pertence
    int r = rand()%somafp, inv = 0;

    for(i=0; i<nobj; i++){
        if(r > fatordp[i]+inv) inv+=fatordp[i];
        else break;
    }

    return i;
}
