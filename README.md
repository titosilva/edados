# edados

Grupo 1 :fire:

BR ARRASAR GLR :star2:

O que cada comando faz?  :confused:

"git clone URL" =
esse comando cria um repositório local no seu computador (uma cópia deste repositório em uma pasta, com as mesmas configurações e tudo mais).  
"git branch" =
esse comando retorna as branchs existentes no seu repositório local e também mostra a branch que você está modificando  
"git branch nome-da-branch" =
esse comando cria uma branch nova, com nome nome-da-branch  
"git checkout nome-da-branch" =
move você para uma outra branch (as alterações de uma branch para outra não são perdidas)  
"git add ." =
faz o stage de todas as alterações que voce fez na pasta atual  
"git commit -m "mensagem"" =
cria um commit com titulo mensagem  
"git push origin nome-da-branch" =
atualiza o diretorio da internet para deixa-lo igual ao seu repositorio local  
"git pull origin nome-da-branch" =
atualiza o seu repositorio local para deixa-lo conforme a branch indicada do repostorio da internet  

Assim, quando for fazer modificações, siga os passos:
1. Crie um repositorio local(isso só precisa ser feito uma vez):

    'git clone URL', onde URL="https://gitlab.com/titosilva/edados"
    
2. entre na pasta que foi criada por meio da linha de comandos

    'cd edados/'
    
3. crie uma branch nova e use ela(convencionamos que o nome da branch que voce criara deve ser o seu nome). Lembre de usar git pull para atualizar seu repositorio local anter de começar:

    'git branch nome-da-branch'  
    'git checkout nome-da-branch'  
    'git pull origin develop'  
    
4. Faça as alterações e dê um stage nelas:

    'git add .'

5. Crie um commit:

    'git commit -m "explicação de quais alterações foram feitas"'
    
6. Atualize o repositorio na internet:

    'git push origin nome-da-sua-branch'
    
7. Vá até o gitlab e solicite um merge request com source branch=sua branch e target branch=develop
8. Avise a galera para vermos o que fazer em seguida
9. Repita isso tudo e arrase no trabalho :sunglasses: