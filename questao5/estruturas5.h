#ifndef ESTRUTURAS5_H_
#define ESTRUTURAS5_H_

/* 
    Declaração de funções e estruturas a serem utilizadas na questao 5
*/

// ARVORE ==========================================================================================
typedef struct arvno_str{
    // Campo chave (inteiros de 1 a 100)
    int chave;
    // Campo extra (numero de ponto flutuante)
    float campoextra;
    // Filhos
    struct arvno_str *d;
    struct arvno_str *e;
} No;


typedef struct arvbin_str{
    // Nó raiz
    No *raiz;
} ArvoreBinaria;

// FUNCOES ==========================================================================================

// Coloca raiz em NULL
void inicializarArvore(ArvoreBinaria *arv);

// Aloca memoria para um no
// Retorna o endereço onde o no foi alocado
No *novoNo(int chave, float campoextra);

// Retorna 0 caso a chave ja esteja na lista;
// Retorna 1 caso contrario
int insercaoEmArvore(ArvoreBinaria *arv, int chave, float campoextra);
int insercaoRecursiva(No *r, No *no);

// Conta quantos nos ha na arvore utilizando a recursividade
int contarNos(ArvoreBinaria *arv);
int contarNosRecursivo(No *r);

// Retorna o campo extra por referencia
// Retorna 1 caso o elemento seja encontrado
// Retorna 0 caso contrario
int verCampoExtra(ArvoreBinaria *arv, int chave, float *retorno);
// funcao auxiliar para verCampoExtra
int verCampoExtraRecursivo(No *r, int chave, float *retorno);

// Libera a memoria alocada para os nos
void finalizarArvore(ArvoreBinaria *arv);
// Função auxiliar para finalizar Arvore
void finalizarRecursivo(No *r);

// Imprime os nos da arvore
void imprimirNos(ArvoreBinaria arv);
// Função auxiliar para a funcao imprimirNos
void imprimirRecursivo(No *r, int profundidade);

#endif