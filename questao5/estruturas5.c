#include "estruturas5.h"
#include <stdio.h>
#include <stdlib.h>

/*
    Implementação das funções declaradas no arquivo estruturas5.h
*/

void inicializarArvore(ArvoreBinaria *arv){
    arv->raiz = NULL;
}

No *novoNo(int chave, float campoextra){
    No *novo = (No*) calloc(1, sizeof(No));
    novo->chave = chave;
    novo->campoextra = campoextra;
    return novo;
}

int insercaoEmArvore(ArvoreBinaria *arv, int chave, float campoextra){
    if(arv->raiz==NULL){
        arv->raiz = novoNo(chave, campoextra);
        return 1;
    }

    // Usa a recursividade para inserir na arvore
    return insercaoRecursiva(arv->raiz, novoNo(chave, campoextra));
}

int insercaoRecursiva(No *r, No *no){
    if(r->chave==no->chave) return 0;
    else{
        if(r->chave>no->chave){
            if(r->e!=NULL){
                if(insercaoRecursiva(r->e, no)==0) return 0;
                else return 1;
            }else{
                r->e = no;
                return 1;
            }
        }else{
            if(r->d!=NULL){
                if(insercaoRecursiva(r->d, no)==0) return 0;
                else return 1;
            }else{
                r->d = no;
                return 1;
            }
        }
    }
}

int contarNos(ArvoreBinaria *arv){
    return contarNosRecursivo(arv->raiz);
}

int contarNosRecursivo(No *r){
    if(r==NULL){
        return 0;
    }
    else{
        return 1 + contarNosRecursivo(r->d) + contarNosRecursivo(r->e);
    }
}

int verCampoExtra(ArvoreBinaria *arv, int chave, float *retorno){
    return verCampoExtraRecursivo(arv->raiz, chave, retorno);
}

int verCampoExtraRecursivo(No* r, int chave, float *retorno){
    // Verifica se aainda há elementos a serem verificados
    if(r==NULL) return 0;

    // Caso ainda hajam elementos a serem verificados,
    // verifica se o proximo elemento ja nao e o procurado
    // se não for, faz uma chamada recursiva para a direita ou para a esquerda, dependendo do caso
    if(r->chave == chave){
        *retorno = r->campoextra;
        return 1;
    }else{
        if(chave < r->chave) return verCampoExtraRecursivo(r->e, chave, retorno);
        else return verCampoExtraRecursivo(r->d, chave, retorno);
    }
}

void finalizarArvore(ArvoreBinaria *arv){
    // Utiliza a recursividade para finalizar a arvore
    finalizarRecursivo(arv->raiz);
}

void finalizarRecursivo(No *r){
    if(r!=NULL){
        finalizarRecursivo(r->d);
        finalizarRecursivo(r->e);
        free(r);
    }
}

void imprimirNos(ArvoreBinaria arv){
    // Usa a recursividade para imprimir os nos
    imprimirRecursivo(arv.raiz, 0);
}

void imprimirRecursivo(No *r, int profundidade){
    // Primeiro, imprime os nos que estao à direita do no passado
    // Depois, imprime os nos que estao à esquerda do no passado
    // A profundidade serve apenas para determinar os espaços

    if(r!=NULL){
        printf("%d\n", r->chave);

        if(r->d!=NULL){
            for(int i=0; i<=profundidade*2; i++){
                // Imprime espaços, para identação
                if(i%2==1) putchar(' ');
                else putchar('|');
            }
            putchar('<');
            imprimirRecursivo(r->d, profundidade+1);
        }
        
        if(r->e!=NULL){    
            for(int i=0; i<=profundidade*2; i++){
                // Imprime espaços, para identação
                if(i%2==1) putchar(' ');
                else putchar('|');
            }
            putchar('>');
            imprimirRecursivo(r->e, profundidade+1);
        }
    }
}