#ifndef ESTRUTURAS6_H_
#define ESTRUTURAS6_H_

/* 
    Declaração de funções e estruturas a serem utilizadas na questao 6
    As estruturas e funções aqui utilizadas sao muito semelhantes às utilizadas na questao 5
*/

// ARVORE ==========================================================================================
typedef struct arvno_str{
    // Campo chave (inteiros de 1 a 100)
    int chave;
    // Campo extra (numero de ponto flutuante)
    float campoextra;
    // Altura do nó
    int altura;
    // Filhos
    struct arvno_str *d;
    struct arvno_str *e;
} No;


typedef struct arvavl_str{
    // Nó raiz
    No *raiz;
} ArvoreAVL;

// FUNCOES ==========================================================================================

// Coloca raiz em NULL
void inicializarArvore(ArvoreAVL *arv);

// Aloca memoria para um no
// Retorna o endereço onde o no foi alocado
No *novoNo(int chave, float campoextra);

// Funções de rotação
// Aplica rotação sobre o nó r e retorna o nó que ocupará o seu lugar
No *rotDireita(No *r);
No *rotEsquerda(No *r);

// Retorna 0 caso a chave ja esteja na lista;
// Retorna 1 caso contrario
int insercaoEmArvoreAVL(ArvoreAVL *arv, int chave, float campoextra);
No *insercaoRecursivaAVL(No *no, int chave, float campoextra);

// Retorna -1 se o no for nulo
// Retorna a altura do no caso contrario
int alturaNo(No *no);

// Conta quantos nos ha na arvore utilizando a recursividade
int contarNos(ArvoreAVL *arv);
int contarNosRecursivo(No *r);

// Retorna o campo extra por referencia
// Retorna 1 caso o elemento seja encontrado
// Retorna 0 caso contrario
int verCampoExtra(ArvoreAVL *arv, int chave, float *retorno);
// funcao auxiliar para verCampoExtra
int verCampoExtraRecursivo(No *r, int chave, float *retorno);

// Libera a memoria alocada para os nos
void finalizarArvore(ArvoreAVL *arv);
// Função auxiliar para finalizar Arvore
void finalizarRecursivo(No *r);

// Imprime os nos da arvore
void imprimirNos(ArvoreAVL arv);
// Função auxiliar para a funcao imprimirNos
void imprimirRecursivo(No *r, int profundidade);

#endif