#include <stdio.h>
#include <stdlib.h>
#include "estruturas6.h"

/*
    Função main da questao 6. O arquivo segue o mesmo formato da questao 5, apenas alterando 
    detalhes para adequar às arvores avl.
*/

// Aparece para o usuario quando ele deve digitar algo
#define SELECAO ">> "

// Função para limpar os conteúdos da tela
void limparTela();
// Pede ao usuario para selecionar uma das opções(menu principal)
int selecao1();
// Menus
void MenuInsercao(ArvoreAVL *arv);
void MenuvisualizarCampoExtra(ArvoreAVL *arv);

int main(int argc, char const *argv[]){
    printf("ARVORES AVL");
    putchar('\n');
    ArvoreAVL bin;
    inicializarArvore(&bin);

    // Variavel para controlar o loop while
    int loop = 1;
    while(loop){
        switch(selecao1()){
        case 1:
            // Caso de insercao de elementos
            limparTela();
            putchar('\n');
            MenuInsercao(&bin);
            break;
        case 2:
            limparTela();
            putchar('\n');
            printf("Ha %d nos na arvore\n", contarNos(&bin));
            break;
        case 3:
            limparTela();
            putchar('\n');
            imprimirNos(bin);
            break;
        case 4:
            limparTela();
            putchar('\n');
            MenuvisualizarCampoExtra(&bin);
            break;
        default:
            loop = 0;
            break;
        }
        putchar('\n');
    }

    finalizarArvore(&bin);
    return 0;
}

void limparTela(){
    #ifdef __unix__
        system("clear");
    #elif __WIN32
        system("cls");
    #endif
}

int selecao1(){
    while(1){
        printf("Selecione uma das opcoes:\n");
        printf("-1 > Finalizar\n");
        printf("1 > Inserir novo elemento (com chave de 1 a 100 + campo extra)\n");
        printf("2 > Contar quantidade de nós na árvore\n");
        printf("3 > Exibir nós\n");
        printf("4 > Visualizar um campo extra\n");
        printf(SELECAO);

        // Pega entrada do usuario e analisa
        while(1){    
            int sel;
            scanf("%d", &sel);
            // Esvazia o buffer
            while(getchar()!='\n');
            switch(sel){
                case -1:
                case 1:
                case 2:
                case 3:
                case 4:
                    return sel;
                    break;
                default:
                    printf("Selecao invalida. Selecione uma das opcoes disponiveis\n");
                    printf(SELECAO);
            }
        }
    }
}

void MenuInsercao(ArvoreAVL *arv){
    printf("Insercao de elemento\n");
    printf("Qual chave voce deseja adicionar?\n");

    int chave;
    while(1){
        printf(SELECAO);
        scanf("%d", &chave);
        // Esvazia buffer
        while(getchar()!='\n');
        if(chave>=1 && chave<=100){
            break;
        }else{
            printf("Chave invalida. Digite apenas valores de 1 a 100\n");
        }
    }

    float campoextra;
    printf("Qual o campo extra do novo elemento?\n");
    printf(SELECAO);
    scanf("%f", &campoextra);
    // Caso o usuário digite algo invalido, o valor
    // sera 0

    // Esvazia buffer
    while(getchar()!='\n');

    if(insercaoEmArvoreAVL(arv, chave, campoextra)==0){
        printf("O elemento com essa chave ja esta na lista!\n");
    }else{
        printf("Elemento adicionado\n");
    }
}

void MenuvisualizarCampoExtra(ArvoreAVL *arv){
    int chave;
    imprimirNos(*arv);
    printf("Qual o campo chave do elemento a ser visualizado?\n");

    while(1){
        printf(SELECAO);
        scanf("%d", &chave);
        // Esvazia buffer
        while(getchar()!='\n');

        if(chave>=1 && chave<=100) break;
        else{
            printf("Chave invalida! Use somente chaves de 1 a 100\n");
        }
    }

    // Armazenara o resultado
    float res;
    if(verCampoExtra(arv, chave, &res)){
        printf("Campo chave:\t%d\nCampo extra:\t%f\n", chave, res);
    }else{
        printf("Chave nao encontrada!\n");
    }
}