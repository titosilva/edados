#include "estruturas6.h"
#include <stdio.h>
#include <stdlib.h>


/*
Implementações das funções declaradas em estruturas6.h
Como as arvores AVL sao um caso específico de arvores de busca binaria,
algumas funções são iguais às implementadas no arquivo estruturas5.c
*/

void inicializarArvore(ArvoreAVL *arv){
    arv->raiz = NULL;
}

No *novoNo(int chave, float campoextra){
    No *novo = (No*) calloc(1, sizeof(No));
    novo->chave = chave;
    novo->campoextra = campoextra;
    novo->altura = 0;
    novo->e = novo->d = NULL;
    return novo;
}

No *rotEsquerda(No *r){
    No *d = r->d;
    // Desloca os nós
    r->d = d->e;
    d->e = r;

    // Calcula novas alturas
    int ne, nd;
    ne = alturaNo(r->e);
    nd = alturaNo(r->d);
    r->altura = (nd > ne)? nd + 1 : ne + 1;

    ne = alturaNo(d->e);
    nd = alturaNo(d->d);
    d->altura = (nd > ne)? nd + 1 : ne + 1;
    
    return d;
}

No *rotDireita(No *r){
    No *e = r->e;
    // Desloca os nós
    r->e = e->d;
    e->d = r;

    // Calcula novas alturas
    int ne, nd;
    ne = alturaNo(r->e);
    nd = alturaNo(r->d);
    r->altura = (nd > ne)? nd + 1 : ne + 1;

    ne = alturaNo(e->e);
    nd = alturaNo(e->d);
    e->altura = (nd > ne)? nd + 1 : ne + 1;
    
    return e;
}

int insercaoEmArvoreAVL(ArvoreAVL *arv, int chave, float campoextra){
    // Guarda o retorno das funcoes
    No *ret;
    ret = insercaoRecursivaAVL(arv->raiz, chave, campoextra);
    if(ret==NULL){
        return 0;
    }else{
        arv->raiz = ret;
        return 1;
    }
}

No *insercaoRecursivaAVL(No *no, int chave, float campoextra){
    if(no==NULL){
        return novoNo(chave, campoextra);
    }

    // Guarda o retorno das funcoes
    No *ret;
    if(no->chave < chave){
        ret = insercaoRecursivaAVL(no->d, chave, campoextra);
        no->d = ret;
    }else if(no->chave > chave){
        ret = insercaoRecursivaAVL(no->e, chave, campoextra);
        no->e = ret;
    }else{
        // Se o elemento ja estiver inserido, nao sera inserido novamente
        return no;
    }

    // Atualiza a altura do nó
    int ne = alturaNo(no->e);
    int nd = alturaNo(no->d);
    no->altura = (ne > nd)? ne + 1 : nd + 1;

    // Como todas as insercoes sao ordenadas, os "netos" do no
    // onde ocorreu o desequilibrio so podem ser um NULL e o outro
    // algum no valido. Usaremos essa propriedade na implementacao
    if(ne-nd >= 2){
        // No da esquerda com altura maior que no da direita
        // Logo, a insercao se deu na esquerda
        if(chave < no->e->chave){
            // Inserção mais "externa" na subarvore
            return rotDireita(no);
        }else{
            // Inserção mais "interna" na subarvore
            no->e = rotEsquerda(no->e);
            return rotDireita(no);
        }
    }else if(ne-nd <= -2){
        // No da direita com altura maior que no da esquerda
        // Logo, a insercao se deu na esquerda
        if(chave > no->d->chave){
            // Inserção mais "externa" na subarvore
            return rotEsquerda(no);
        }else{
            // Inserção mais "interna" na subarvore
            no->d = rotDireita(no->d);
            return rotEsquerda(no);
        }
    }

    return no;
}

int alturaNo(No *no){
    if(no==NULL) return -1;
    else return no->altura;
}

int contarNos(ArvoreAVL *arv){
    return contarNosRecursivo(arv->raiz);
}

int contarNosRecursivo(No *r){
    if(r==NULL){
        return 0;
    }
    else{
        return 1 + contarNosRecursivo(r->d) + contarNosRecursivo(r->e);
    }
}

int verCampoExtra(ArvoreAVL *arv, int chave, float *retorno){
    return verCampoExtraRecursivo(arv->raiz, chave, retorno);
}

int verCampoExtraRecursivo(No* r, int chave, float *retorno){
    // Verifica se aainda há elementos a serem verificados
    if(r==NULL) return 0;

    // Caso ainda hajam elementos a serem verificados,
    // verifica se o proximo elemento ja nao e o procurado
    // se não for, faz uma chamada recursiva para a direita ou para a esquerda, dependendo do caso
    if(r->chave == chave){
        *retorno = r->campoextra;
        return 1;
    }else{
        if(chave < r->chave) return verCampoExtraRecursivo(r->e, chave, retorno);
        else return verCampoExtraRecursivo(r->d, chave, retorno);
    }
}

void finalizarArvore(ArvoreAVL *arv){
    // Utiliza a recursividade para finalizar a arvore
    finalizarRecursivo(arv->raiz);
}

void finalizarRecursivo(No *r){
    if(r!=NULL){
        finalizarRecursivo(r->d);
        finalizarRecursivo(r->e);
        free(r);
    }
}

void imprimirNos(ArvoreAVL arv){
    // Usa a recursividade para imprimir os nos
    imprimirRecursivo(arv.raiz, 0);
}

void imprimirRecursivo(No *r, int profundidade){
    // Primeiro, imprime os nos que estao à direita do no passado
    // Depois, imprime os nos que estao à esquerda do no passado
    // A profundidade serve apenas para determinar os espaços

    if(r!=NULL){
        printf("%d(h=%d)\n", r->chave, alturaNo(r));

        if(r->d!=NULL){
            for(int i=0; i<=profundidade*2; i++){
                // Imprime espaços, para identação
                if(i%2==1) putchar(' ');
                else putchar('|');
            }
            putchar('<');
            imprimirRecursivo(r->d, profundidade+1);
        }
        
        if(r->e!=NULL){    
            for(int i=0; i<=profundidade*2; i++){
                // Imprime espaços, para identação
                if(i%2==1) putchar(' ');
                else putchar('|');
            }
            putchar('>');
            imprimirRecursivo(r->e, profundidade+1);
        }
    }
}