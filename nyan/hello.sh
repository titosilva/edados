#!/bin/bash
# Imprime varios "hello" em lugares aletorios da tela

# Associa CRTL-C à finalização do programa
trap 'end' 2
end(){
    tput cnorm
    clear 
    tput cup 0 0 
    tput setaf 7
    echo 'BYE :)'
    exit 1
}

clear
# Pega o tamanho da tela
cols=$(tput cols)
lines=$(tput lines)
# Numero de impressoes
num=600
# Texto
msg="BOM DIA"
msgsize=${#msg}
# Espaçamento para regular as impressoes na tela
offset=5
# Pequeno intervalo entre uma mensagem e outra
inter=0.005
# Intervalo entre as impressoes e o final
pause=1

#Desabilitar cursor
tput civis

for (( i=0; i<$num; i=i+1 ))
do
    tput cup $(( $RANDOM%($lines-$offset) + $offset/2 )) $(( $RANDOM%($cols) ))
    tput setaf $(( $RANDOM%7 + 1 ))
    echo "$msg"
    sleep $inter
done

# bom dia
tput setaf 1
qlinename=$( cat "bomdia.txt" | wc -l )
for (( i=1; i<=$qlinename; i=i+1 ))
do
    line=$( head -n $i "bomdia.txt" | tail -n +$i )
    sizeofname=${#line}
    tput cup $(( $lines/2 -$qlinename/2 + $i + 1 )) $(( $cols/2 - $sizeofname/2 ))
    printf "$line"
done

read
clear

# NOMES
tput setaf 2
# tito
qlinename=$( cat tito.txt | wc -l )
for (( i=1; i<=$qlinename; i=i+1 ))
do
    line=$( head -n $i tito.txt | tail -n +$i )
    sizeofname=${#line}
    tput cup $(( $lines/2 -$qlinename/2 + $i + 1 )) $(( $cols/2 - $sizeofname/2 ))
    printf "$line"
done

read
clear

# alex
qlinename=$( cat alex.txt | wc -l )
for (( i=1; i<=$qlinename; i=i+1 ))
do
    line=$( head -n $i alex.txt | tail -n +$i )
    sizeofname=${#line}
    tput cup $(( $lines/2 -$qlinename/2 + $i + 1 )) $(( $cols/2 - $sizeofname/2 ))
    printf "$line"
done

read
clear

# amanda
qlinename=$( cat amanda.txt | wc -l )
for (( i=1; i<=$qlinename; i=i+1 ))
do
    line=$( head -n $i amanda.txt | tail -n +$i )
    sizeofname=${#line}
    tput cup $(( $lines/2 -$qlinename/2 + $i + 1 )) $(( $cols/2 - $sizeofname/2 ))
    printf "$line"
done

read
clear

# gabriel
qlinename=$( cat gabriel.txt | wc -l )
for (( i=1; i<=$qlinename; i=i+1 ))
do
    line=$( head -n $i gabriel.txt | tail -n +$i )
    sizeofname=${#line}
    tput cup $(( $lines/2 -$qlinename/2 + $i + 1 )) $(( $cols/2 - $sizeofname/2 ))
    printf "$line"
done

read
clear

# lucas
qlinename=$( cat lucas.txt | wc -l )
for (( i=1; i<=$qlinename; i=i+1 ))
do
    line=$( head -n $i lucas.txt | tail -n +$i )
    sizeofname=${#line}
    tput cup $(( $lines/2 -$qlinename/2 + $i + 1 )) $(( $cols/2 - $sizeofname/2 ))
    printf "$line"
done

read
clear

# Imprime o arco iris atras do nyan cat
qlinerainb=$( cat rainbow.txt | wc -l )
nyanY=20
sizeofrainb=0
for (( i=1; i<$qlinerainb; i=i+1 ))
do
    line=$( head -n $i rainbow.txt | tail -n +$i )
    sizeofrainb=${#line}
    tput cup $(( $nyanY + $i + 1 )) 0
    tput setaf $(( ($i-1)/2+1 ))
    printf "$line"
done

# Imprime o nyan cat
qlinenyan=$( cat nyan.txt | wc -l )
tput setaf 7
for (( i=1; i<$qlinenyan; i=i+1 ))
do
    line=$( head -n $i nyan.txt | tail -n +$i )
    tput cup $(( $nyanY + $i )) $(( $sizeofrainb + 1 ))
    printf "$line"
done

# Imprime "GRUPO 1"
qline=$( cat grupo1.txt | wc -l )
#Posicao
Y=8
# Cores
r=255; g=0; b=0;
step=0
cores=( 255 50 255 1 255 1 1 1 255 204 102 255 153 102 51 )
coreslen=5
for (( k=0; ; k=(k+1)%$coreslen ))
do
    for (( c=0; c<255; c++ ))
    do
        r=$(( $c>${cores[$k*3]}? ${cores[$k*3]} : $c ))
        g=$(( $c>${cores[$k*3+1]}? ${cores[$k*3+1]} : $c ))
        b=$(( $c>${cores[$k*3+2]}? ${cores[$k*3+2]} : $c ))
        for (( i=1; i<$qline; i=i+1 ))
        do
            line=$( head -n $i grupo1.txt | tail -n +$i )
            tput cup $(( $Y + $i )) $(( $cols/2 - ${#line}/2 ))
            format="\033[38;2;${r};${g};${b}m$line\n"
            printf "$format"
        done

    done
    for (( c=255; c>0; c-- ))
    do
        r=$(( $c>${cores[$k*3]}? ${cores[$k*3]} : $c ))
        g=$(( $c>${cores[$k*3+1]}? ${cores[$k*3+1]} : $c ))
        b=$(( $c>${cores[$k*3+2]}? ${cores[$k*3+2]} : $c ))
        for (( i=1; i<$qline; i=i+1 ))
        do
            line=$( head -n $i grupo1.txt | tail -n +$i )
            tput cup $(( $Y + $i )) $(( $cols/2 - ${#line}/2 ))
            format="\033[38;2;${r};${g};${b}m$line\n"
            printf "$format"
        done
    done
done

# Restaura o cursor
tput cnorm
