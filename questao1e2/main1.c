#include <stdlib.h>
#include "estruturas1.h"
#include <stdio.h>

void clearscreen()
{
#ifdef __unix__
    system("clear");
#elif _WIN32
    system("cls");
#elif __linux__
    system("clear");
#endif
}

int main()
{
    listaencadeada lista;
    pilhaestatica pilha;
    registro auxiliar;
    elemento teste;
    // Indica se ja ocorreu uma inicializacao
    int inicializado = 0;

    int opcao = 0, aux = 0, a = 0;

    printf("Qual estrutura voce deseja usar?\n1-Lista encadeada\n2-Pilha estatica\n");

    scanf("%d", &opcao);
    clearscreen();

    // Esvazia buffer
    while (getchar() != '\n')
        ;

    switch (opcao)
    {
    case 1:
        while (aux != -1)
        {
            printf("O que voce deseja fazer?\n1->Inicializar a lista\n2->Retornar qtd de elementos validos\n3->Imprimir todas as chaves\n4->Inserir uma chave\n5->Eliminar um elemento\n6->Reinicializar a lista\n-1->Sair\n");
            scanf("%d", &aux);
            clearscreen();

            // Esvazia buffer
            while (getchar() != '\n')
                ;

            switch (aux)
            {
            case 1:
                if(!inicializado){
                    inicializarLista(&lista);
                    clearscreen();
                    printf("Lista inicializada!\n\n");
                    inicializado = 1;
                }else{
                    printf("Lista ja inicializada\n");
                }

                break;
            case 2:
                if(inicializado){
                    a = tamanhoLista(&lista);
                    clearscreen();
                    printf("A lista tem tamanho %d.\n\n", a);
                }else{
                    printf("Inicialize a lista antes de efetuar outras operações\n");
                }

                break;
            case 3:
                if(inicializado){
                    mostrarLista(&lista);
                }else{
                    printf("Inicialize a lista antes de efetuar outras operações\n");
                }

                break;
            case 4:
                if(inicializado){    
                    printf("Digite a chave que você deseja inserir: ");
                    scanf("%d", &teste.reg.chave);
                    a = inserirChave(&lista, teste.reg.chave);
                    clearscreen();
                    if (a == 1)
                    {
                        printf("Chave inserida com sucesso!\n\n");
                    }
                }else{
                    printf("Inicialize a lista antes de efetuar outras operações\n");
                }
                break;
            case 5:
                if(inicializado){
                    printf("Qual elemento você deseja excluir?\n");
                    mostrarLista(&lista);
                    printf("\n");
                    scanf("%d", &teste.reg.chave);
                    clearscreen();
                    if (!eliminarElemento(&lista, teste.reg.chave))
                    {
                        printf("Não foi possivel encontrar o elemento!\n");
                    }
                    else{
                        printf("Elemento excluído com sucesso!\n\n");
                    }
                }else{
                    printf("Inicialize a lista antes de efetuar outras operações\n");
                }
                

                break;
            case 6:
                if(inicializado){
                    reinicializarLista(&lista);
                    clearscreen();
                    printf("Lista reinicializada!\n\n");
                }else{
                    printf("Inicialize a lista antes de efetuar outras operações\n");
                }

                break;
            default:
                if (aux != -1)
                {
                    clearscreen();
                    printf("Opcao invalida!\n");
                }

                break;
            }
        }

        break;

    case 2:
        aux = 0;
        while (aux != -1)
        {
            printf("O que voce deseja fazer?\n1->Inicializar a pilha\n2->Retornar qtd de elementos validos\n3->Imprimir todas as chaves\n4->Inserir um novo elemento\n5->Eliminar um elemento da pilha\n6->Reinicializar a pilha\n-1->Sair\n");
            scanf("%d", &aux);
            clearscreen();

            // Esvazia buffer
            while (getchar() != '\n')
                ;
            switch (aux)
            {
            case 1:
                if(!inicializado){
                    inicializarPilha(&pilha);
                    clearscreen();
                    printf("Pilha inicializada!\n\n");
                    inicializado = 1;
                }else{
                    printf("Pilha ja inicializada\n");
                }

                break;
            case 2:
                if(inicializado){
                    a = tamanhoPilha(&pilha);
                    clearscreen();
                    printf("A pilha tem tamanho %d.\n\n", a);
                }else{
                    printf("Inicialize a pilha antes de realizar outras operacoes\n");
                }

                break;
            case 3:
                if(inicializado){
                    a = mostrarPilha(&pilha);
                    if (a == 0)
                    {
                        printf("A pilha está vazia!\n\n");
                    }
                }else{
                    printf("Inicialize a pilha antes de realizar outras operacoes\n");
                }

                break;
            case 4:
                if(inicializado){
                    printf("Digite o elemento que você deseja adicionar:\n");
                    scanf("%d", &teste.reg.chave);
                    clearscreen();
                    a = push(&pilha, teste.reg.chave);
                    if (a == 0)
                    {
                        printf("Não foi possível adicionar o elemento pois a pilha está cheia");
                    }
                    else
                    {
                        printf("Elemento adicionado com sucesso!!\n\n");
                    }
                }else{
                    printf("Inicialize a pilha antes de realizar outras operacoes\n");
                }

                break;
            case 5:
                if(inicializado){
                    a = pop(&pilha, &auxiliar);
                    clearscreen();
                    if (a == 0)
                    {
                        printf("Não há o que excluir. A pilha está vazia!!\n\n");
                    }
                    else if (a == 1)
                    {
                        printf("Elemento excluido com sucesso!\n\n");
                    }
                }else{
                    printf("Inicialize a pilha antes de realizar outras operacoes\n");
                }

                break;
            case 6:
                if(inicializado){
                    reinicializarPilha(&pilha);
                    clearscreen();
                }else{
                    printf("Inicialize a pilha antes de realizar outras operacoes\n");
                }

                break;
            default:
                if (aux != -1)
                {
                    clearscreen();
                    printf("Opcao invalida!\n");
                }

                break;
            }
        }

        break;

    default:
        while (opcao != 1 && opcao != 2)
        {
            printf("%d Nao e uma opcao!\n", opcao);
            printf("Qual estrutura voce deseja usar?\n1-Lista encadeada\n2-Pilha estatica\n");
            scanf("%d", &opcao);
        }
        break;
    }

    return 0;
}
