#include <stdlib.h>
#include <stdio.h>
#include "estruturas1.h"

void inicializarLista(listaencadeada *lista)
{
    lista->inicio = NULL;
}

int tamanhoLista(listaencadeada *lista)
{

    //cria um novo elemento para guardar o elemento que está no início da lista
    elemento *end = lista->inicio;
    int tamanho = 0;

    //enquanto não chegar no último elemento que contém null no campo prox
    //o tamanho da lista é incrementado
    while (end != NULL)
    {
        tamanho++;
        end = end->prox;
    }

    return tamanho;
}

void mostrarLista(listaencadeada *lista)
{
    elemento *end = lista->inicio;
     while (end != NULL)
    {
        printf("%d\n", end->reg.chave);
        end = end->prox;
    }
}

elemento *buscaSequencialExc(listaencadeada *lista, int ch, elemento **ant)
{                                    // Função que retorna o endereço do elemento buscado
                                     // recebe um ponteiro pra lista, o valor a ser proucurado e um ponteiro pra ponteiro, onde esse recebe o endereço de memoria onde deverá ser colocado o endereço anterior
    *ant = NULL;                     // porque se esta no começo da lista o elemento anterior não é valido
    elemento *atual = lista->inicio; // é colocado o endereço do inicio da lista em atual
    while ((atual != NULL) && (atual->reg.chave < ch))
    {
        //Esse while serve pra passarmos pela lista, em quanto o valor da chave atual for menor que o valor que esta sendo procurado
        *ant = atual;
        atual = atual->prox;
    }
    if ((atual != NULL) && (atual->reg.chave == ch))
        return atual; // Se for achado um elemento com o valor a ser proucurado, retornar o endereço desse.
    return NULL;      //No caso do elemento não ser encontrado retornar NULL
}

// retorna 1 se foi possivel inserir o elemento, e 0 caso o contrário
int inserirChave(listaencadeada *lista, int chave)
{ // Função que insere uma chave que o usuário pediu pra inserir
    elemento *ant, *i;
    i = buscaSequencialExc(lista, chave, &ant); // atribuir ao valor de de i o endereço da do elemento proucurado (se ele existir), ou NULL em caso do elemento não ser encontrado
    if (i != NULL)
        return 0;                             // Se for diferente de NULL significa que o elemento já existe
    i = (elemento *)malloc(sizeof(elemento)); // Alocando memoria
    i->reg.chave = chave;                     // registro que o usuário pediu pra inserir
    if (ant == NULL)
    {                            // No caso do elemento não ter anterior (ou seja ele é o primeiro elemento)
        i->prox = lista->inicio; // Nesse caso o proximo da lista é o antigo primeiro
        lista->inicio = i;       // Colocando o endereço dele em no ponteiro pra inicio da lista.
    }
    else
    {                        // No caso onde ele não é o primeiro elemento
        i->prox = ant->prox; // Colocando o endereço do proximo elemento do elemento anterior dele
        ant->prox = i;       // Colocando o endereço do atual no proximo do anterior.
    }
    return 1; // Pra representar que foi possivel implementar o elemento.
}

// retorna 1 se o elemento foi encontrado e foi possivel remove-lo
// retorna 0 caso contrario
int eliminarElemento(listaencadeada *lista, int chave)
{
    // Ponteiro que varia ate achar a chave
    elemento *pos = lista->inicio;
    // Ponteiro para o elemento anterior
    elemento *ant = NULL;

    while (pos != NULL)
    {
        if (pos->reg.chave == chave)
        {
            if (ant == NULL)
            {
                // Se ant for NULL significa que o primeiro elemento ser removido
                // Nesse caso, o inicio da lista passa a ser o segundo elemento
                lista->inicio = pos->prox;
            }
            else
            {
                ant->prox = pos->prox;
            }
            free(pos);
            return 1;
        }
        else
        {
            ant = pos;
            pos = pos->prox;
        }
    }

    // Se a execucao chegar a esse ponto, o elemento nao foi encontrado
    return 0;
}

void reinicializarLista(listaencadeada *lista)
{                                  // Função pra reiniciar a lista
    elemento *end = lista->inicio; // colocando em end, o endereço para o primeiro elemento da lista
    while (end != NULL)
    {                           // while pra passar pela lista
        elemento *apagar = end; // apagar é uma variavel auxiliar, essa que apagaremos
        end = end->prox;        // mudando o endereço de end pra proximo para percorrer a lista
        free(apagar);           // liberando a memoria de apagar
    }
    lista->inicio = NULL; // Colocando o inicio em NULL pra representar que a lista não tem elementos validos
}

// Colocar topo em -1;
void inicializarPilha(pilhaestatica *pilha)
{                     // função pra inicializar a pilha
    pilha->topo = -1; // Colocando o topo de pilha em -1 pra representar que não tem elementos na pilha
}

int tamanhoPilha(pilhaestatica *pilha)
{
    return pilha->topo + 1;
}

// Padrao: imprimindo do topo a base ( pra deixar bonitinho 'u' )
int mostrarPilha(pilhaestatica *pilha)
{
    if (pilha->topo == -1)
    {
        return 0;
    }
    // Usando escape sequence para colorir os textos
    printf("\033[01;91mTOPO\n");
    for (int i = pilha->topo; i >= 0; i--)
    {
        printf("\033[32m%d\n", pilha->elementos[i].reg.chave);
    }
    printf("\033[01;91mBASE");
    printf("\033[0m\n");
    return 1;
}

// inserir elemento no topo
int push(pilhaestatica *pilha, int chave)
{

    //se a pilha estiver cheia retorna 0
    if (pilha->topo == MAXPILHA - 1)
    {
        return 0;
    }
    //incrementa 1 ao topo da pilha
    pilha->topo = pilha->topo + 1;
    //coloca a chave passada pelo usuário no topo da pilha
    pilha->elementos[pilha->topo].reg.chave = chave;

    //retorna 1 se o elemento foi inserido
    return 1;
}

// remover elemento do topo e salvar em outro lugar
// retorna 1 se foi possivel remover um elemento
// retorn 0 se houve algum problema
int pop(pilhaestatica *pilha, registro *destino)
{

    // Se nao houver elementos na lista, para a execucao da funcao
    // e retorna 0
    if (pilha->topo == -1)
        return 0;

    // Caso contrario, salva o registro no destino e retorna 1
    // Tambem decrementa o valor do topo
    *destino = pilha->elementos[pilha->topo].reg;
    pilha->topo--;
    return 1;
}

void reinicializarPilha(pilhaestatica *pilha)
{
    pilha->topo = -1;
    //really?? :((
}
