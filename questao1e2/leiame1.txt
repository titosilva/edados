/---------------------------------------------------------------------------\
|                               ++GRUPO 1++                                 |
| **Integrantes:                                                            |
| Tito Silva                                                                |
| Lucas                                                                     |
| Alexandre                                                                 |
| Amanda                                                                    |
| Gabriel                                                                   |
|                                                                           |
| QUESTÃO 1: implementação de lista encadeada e pilha estatica              |
|                                                                           |
| Seguem, abaixo, as explicações sobre cada função.                         |
\---------------------------------------------------------------------------/

================================== ESTRUTURAS ===============================
Utilizamos 3 estruturas para lista encadeada, declaradas em "estruturas1.h":
> registro: contem apenas um campo chave, a ser utilizado em cada elemento
> elemento: contem um ponteiro para o próximo elemento e um registro
> listaencadeada: contem apenas um ponteiro para o primeiro elemento da lista

Utilizamos 1 estrutura para pilha estatica:
> pilhaestatica: contem um vetor de elementos(fizemos isso para reaproveitar a estrutura "elemento").
Também contem um int topo, usado para armazenar a posição do topo da pilha.

Algoritmos e complexidades(implementações no arquivo "estruturas1.c"):
>> void inicializarLista(listaencadeada *lista);
   - Coloca inicio da lista em NULL
   - Complexidade: O(1)

>> int tamanhoLista(listaencadeada *lista);
   - Passa por todos os elementos da lista até achar seu final, fazendo a contagem
   dos elementos. Retorna o resultado da contagem.
   - Complexidade: O(n), onde n é a quantidade de elementos na lista

>> void mostrarLista(listaencadeada *lista);   
   - Passa por todos os elementos da lista, exibindo os valores de suas chaves
   - Complexidade: O(n), onde n é a quantidade de elementos na lista

>> elemento* buscaSequencialExc(listaencadeada* lista, int ch, elemento** ant);
>> int inserirChave(listaencadeada *lista, int chave);

>> int eliminarElemento(listaencadeada *lista, int chave);
   - Passa por todos os elementos da lista, buscando pelo elemento com campo chave desejado.
   Se o elemento é encontrado, é removido e a função retorna 1. Caso contrario, retorna 0.
   Caso o elemento seja encontrado e removido, retorna 1. Retorna 0 no caso contrário.
   - Complexidade: O(n), onde n é a quantidade de elementos na lista. O pior caso ocorre quando
   o elemento nao está na lista

>> void reinicializarLista(listaencadeada *lista);
   - Passa por todos os elementos da lista, liberando a memória ocupada. Posteriormente,
   coloca o inicio da lista em NULL.
   - Complexidade: O(n), onde n é a quantidade de elementos na lista

>> void inicializarPilha(pilhaestatica *pilha);
   - Coloca o topo da pilha em -1
   - Complexidade: O(1)

>> int tamanhoPilha(pilhaestatica *pilha);
   - Retorna topo+1, que corresponde à quantidade de elementos válidos na pilha
   - Complexidade: O(1)

>> int mostrarPilha(pilhaestatica *pilha);
   - Imprime os elementos da pilha, começando do topo até chegar à base. Retorna 0 se não
   houver elementos na lista. Retorna 1 caso haja elementos na lista.
   - Complexidade: O(n), onde n é a quantidade de elementos na lista
   
>> int push(pilhaestatica *pilha, int chave);
   - Adiciona elemento na pilha. Retorna 0 caso a pilha esteja cheia.
   - Complexidade: O(1)

>> int pop(pilhaestatica *pilha, registro* destino);
   - Remove elemento da pilha. Retorna 1 se foi possivel remover o elemento.
   Salva o elemento removido no endereco apontado por "destino".
   - Complexidade: O(1)

>> void reinicializarPilha(pilhaestatica *pilha);
   - Coloca o topo da pilha em -1
   - Complexidade: O(1)
