// Esse arquivo contem a implementacao das estruturas que serao utilizadas no trabalho

// Verificar se o arquivo ja havia sido incluido previamente
#ifndef ESTRUTURAS_G1_H
#define ESTRUTURAS_G1_H

#define MAXPILHA 20

// Registro que sera utilizado
typedef struct registrostr{
    int chave;
} registro;

// Elemento das listas
typedef struct elementostr{
    registro reg;
    // Ponteiro para o proximo elemento
    struct elementostr *prox;
} elemento;


// Lista Encadeada ========================================================================
// Faremos uma lista encadeada linear
typedef struct lista_enc_str{
    // Ponteiro para o primeiro elemento
    elemento *inicio;
} listaencadeada;

void inicializarLista(listaencadeada *lista);
int tamanhoLista(listaencadeada *lista);
void mostrarLista(listaencadeada *lista);   
int inserirChave(listaencadeada *lista, int chave);
int eliminarElemento(listaencadeada *lista, int chave);
void reinicializarLista(listaencadeada *lista);
elemento* buscaSequencialExc(listaencadeada* lista, int ch, elemento** ant);

// Pilha ===================================================================================
typedef struct pilhaestaticastr{
    elemento elementos[MAXPILHA];
    int topo;
} pilhaestatica;

// Colocar topo em -1;
void inicializarPilha(pilhaestatica *pilha);
int tamanhoPilha(pilhaestatica *pilha);
// Lembrar: Indique o sentido da impressao
// (Se foi da base ao topo ou do topo a base)
int mostrarPilha(pilhaestatica *pilha);
// inserir elemento no topo
int push(pilhaestatica *pilha, int chave);
// remover elemento do topo e salvar em outro lugar
int pop(pilhaestatica *pilha, registro* destino);
void reinicializarPilha(pilhaestatica *pilha);

#endif
